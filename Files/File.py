import datetime
import sys
import os

sys.path.append(os.path.realpath('./Lib'))
sys.path.append(os.path.realpath('./Controller'))
import glob
from parameter import *
import json

class FileClass():
    def __init__(self):
        para=parameterClass()
        self.path=para.path_to_models
    def load_File(self,modelname):
        try:
            dict={}
            pathfull=os.path.join(self.path,modelname+'/'+modelname+'.json')
            with open(pathfull, 'r') as f:
                    dict=json.loads(f.read())
            return dict
        except:
            return -1
    def load_model_infor(self,modelname):
        dict=self.load_File(modelname)
        countOfCode=dict['Count']
        CountOfProduct=dict['CountOfProduct']
        rects=dict['Rects']
        deep =dict['deep']
        path_template=dict['Template']
        ch1= dict['Light']['CH1']
        ch2=dict['Light']['CH2']
        ExposureTime=dict['camera']['ExposureTime']
        Gamma=dict['camera']['Gamma']
        Z_pulse=dict['Pulse']['Z_value']
        Y_pulse= dict['Pulse']['Y_value']
        Distance_trigger=dict['PLC']['Distance_trigger']
        return countOfCode,CountOfProduct,rects,deep,path_template,ch1,ch2,ExposureTime,Gamma,Z_pulse,Y_pulse,Distance_trigger

    def load_List_Models(self):
        dir=glob.glob(self.path+'/*')
        listmodel= [i.split('/')[-1] for i in dir]
        # print(listmodel)
        return listmodel
    def save_File(self,modelName,count,CountOfProduct,rects,Template,deep,CH1,CH2,ExposureTime,Gamma,Z_value,Y_value,Distance_trigger):
        path1=os.path.join(self.path,modelName+"/"+modelName+".json")
        try:
            with open(path1, 'r+') as file:
                dict=json.loads(file.read())
                dict['Count']=count
                dict['CountOfProduct']=CountOfProduct

                dict['Rects']=rects
                dict['Template'] = Template
                dict['deep']=deep
                dict['modifiled']= str(datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                dict['Light']['CH1']=CH1
                dict['Light']['CH2']=CH2
                dict['PLC']['Z_value'] = Z_value
                dict['PLC']['Y_value'] = Y_value
                dict['PLC']['Distance_trigger'] = Distance_trigger
                dict['camera']['ExposureTime']=ExposureTime
                dict['camera']['Gamma']=Gamma
                file.seek(0)
                json.dump(dict, file, indent=4)
                file.truncate()
                return 1
        except:
            return -1
    def create_File_Json_models(self,path,name):
            dictcam={}
            dictcam['ExposureTime']=3000
            dictcam['Gamma']=1
            dictlight={}
            dictlight['CH1']=127
            dictlight['CH2']=127
            dictPLC={}
            dictPLC['Z_value'] = 0
            dictPLC['Y_value'] = 30000
            dictPLC['Distance_trigger'] = 0
            dict = {}
            dict['name'] = name
            dict['Count']=0
            dict['CountOfProduct']=1

            dict['Rects']=[]
            dict['Template'] = ''
            dict['deep'] = 10
            dict['modifiled'] = ''
            dict['created'] = str(datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
            dict['camera'] = dictcam
            dict['Light'] = dictlight
            dict['PLC'] = dictPLC
            with open(path, "w") as f:
                json.dump(dict,f)

    def create_model(self,listModel):
        parent_dir = "/home/"
        path1 = os.path.join(parent_dir,"pi")
        if not os.path.exists(path1):
            os.mkdir(path1)
        path2 = os.path.join(path1,"LinkCode") 
        if not os.path.exists(path2):
            os.mkdir(path2)
        path3 = os.path.join(path2,"models")
        if not os.path.exists(path3):
            os.mkdir(path3)
        if len(listModel)>0:
            for i in listModel:          
                # parent_dir = parameter.path_to_models
                path = os.path.join(path3,i) 
                if not os.path.exists(path):
                    os.mkdir(path)
                path1 = os.path.join(path,i+'.json') 
                if not os.path.exists(path1):
                    self.create_File_Json_models(path1,i)