
import requests
import json
import time

class Http_client_Class():
    def __init__(self,ip,port):
        self.url="http://"+ip+":"+str(port)
    def get(self,param):
        check=False
        jsontext=''
        try:
            url = self.url+"/"+param
            payload={}
            headers = {
                'id':'link'
            }
            response = requests.request("GET", str(url), headers=headers, data=payload,verify=False,timeout=.5)
            if response!='':
                jsontext=json.loads(response.text)
                check=True
            
        except:
                check=False

        return jsontext,check

# c=Http_client_Class("192.168.1.100",8888)

# b,f=c.get('users')
# a,f=c.get('codhgje')
# print(b,f)
# c.get('snbox')
