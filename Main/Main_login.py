import sys
import os
from PyQt5 import QtWidgets

sys.path.insert(0,os.path.realpath(r'/home/pi/autoLink/Controller'))
sys.path.insert(0,os.path.realpath(r'/home/pi/autoLink/Views'))
sys.path.insert(0,os.path.realpath(r'/home/pi/autoLink/Lib'))
sys.path.insert(0,os.path.realpath(r'/home/pi/autoLink/Files'))

from Main_infor import Main_model_infor
from linkCodeController import ControlerClass
from PyQt5.QtWidgets import QApplication,QDialog, QMessageBox
from UI_login import Ui_login

class Main_login():
    def __init__(self):
        self.dialog = QDialog()
        self.uic = Ui_login()
        self.uic.setupUi(self.dialog)
        self.logctrl=ControlerClass()
        self.event()
    def login(self):
        user=self.uic.lineEdit_user.text()
        password = self.uic.lineEdit_password.text()
        listuser= self.logctrl.get_users()
        # if len(listuser)==0:
        #     msg = QtWidgets.QMessageBox()
        #     msg.setIcon(QtWidgets.QMessageBox.Warning)
        #     msg.setText("Can't login because can't connect to server!")
        #     msg.setWindowTitle("Error")
        #     msg.exec_()
        # else:
            # for u in listuser: 
            #     username=u['User']
            #     passw=u['PassWord']
            # if username==user and passw==password: 
        if user=='a' and password=='123': 
            self.show_model_infor()
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Critical)
            msg.setText("Username or password is not valid!")
            msg.setWindowTitle("Error")
            msg.exec_()
    def event(self):
        self.uic.btnLogin.clicked.connect(self.login)
        self.uic.btnCancel.clicked.connect(self.cancel)
    def cancel(self):
           self.dialog.close()
    def show(self):
        self.dialog.exec()
    def show_model_infor(self):
            self.dialog.close()
            a=Main_model_infor()
            a.show()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    dialog = Main_login()
    dialog.show()
    sys.exit(app.exec())
  