from datetime import datetime
import sys
import os
import threading
import time
from PyQt5 import QtWidgets
from PyQt5.QtCore import QTimer, Qt
import numpy as np

sys.path.append(os.path.realpath('./Controller'))
sys.path.append(os.path.realpath('./Views'))
sys.path.append(os.path.realpath('./Lib'))
sys.path.append(os.path.realpath('./Files'))
from HikCamera import HikCamera, KeyName
from parameter import *
from LightAction import LightAction
from PLCAction import PLC_Action_Class
from UI_home import Ui_LinkCode
import cv2
from linkCodeController import ControlerClass
from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication, QComboBox,QMainWindow, QMessageBox
from Main_login import Main_login
from Image_viewer import ImageViewerClass
class MainWindow():
        def __init__(self):
                self.main_win = QMainWindow()
                self.uic = Ui_LinkCode()
                self.uic.setupUi(self.main_win)
                mainwin=QMainWindow()
                self.imageview=ImageViewerClass(mainwin)
                self.uic.vlayout_imageview.addWidget(self.imageview)
                self.parameter=parameterClass()
                self.main_win.closeEvent=self.closeEvent
                self.controler=ControlerClass()
                self.plc=PLC_Action_Class(self.parameter.plc_ip,self.parameter.plc_port)
                self.camera=HikCamera()
                self.light=LightAction(self.parameter.light_portname)

                self.listcode=[]
                 # count of code
                self.countOfCode=0
                self.deep_model=0
                self.rects=[]
                #camera parameter
                self.ExposureTime=0
                self.Gamma=0
                # Light parameter
                self.ch1=0
                self.ch2=0
                #PLC parameter
                self.Y_pulse=0
                self.Z_pulse=0
                self.Distance_trigger=0
                # RUN    
                self._Run=False
                self.path_template=''
                #
                self.countPass=0
                self.countFail=0
                self.total=0
                self.codeOfBox=''
                self.count_product=0
                self.totalCountOfProduct=0
                # 
                self.isGrab = False
                self.timer=QTimer(self.main_win)
                self.event()
        def event(self):
                self.Load_list_Model_for_Cbbox()
                self.load_model_infor()
                self.uic.cbListmodel.activated.connect(self.load_model_infor)
                self.uic.btnStart.clicked.connect(self.main)
                self.uic.btnModel.clicked.connect(self.btn_Model_Clicked)
                self.uic.cbListmodel.setEditable(True)
                self.uic.cbListmodel.completer().setCompletionMode(QtWidgets.QCompleter.PopupCompletion)
                self.uic.cbListmodel.setInsertPolicy(QComboBox.NoInsert)
                self.check_PLC(False)
                self.check_camera(False)
                self.check_light(False)
                self.check_server(False)
        def closeEvent(self, event):
                self.stop_main()
                close = QMessageBox()
                close.setText("Do you want to exit?")
                close.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
                close = close.exec()
                if close == QMessageBox.Yes:
                        self.close_control()
                        event.accept()
                else:
                        event.ignore()
        def reset(self):
                self.count_product=0

        def close_control(self):
                self.camera.StopGrabbing()
                self.camera.StopCamera()
                self.light.Turn_Of_Light12()
                self.light.close() 
                self.plc.setup_bit_OFF()
        def clear_code(self):
                for i in reversed(range(self.uic.vLayout.count())): 
                        self.uic.vLayout.itemAt(i).widget().deleteLater()
        def add_label_code(self,code):
                font = QtGui.QFont()
                font.setPointSize(13)
                font.setBold(True)
                font.setWeight(75)
                lbCode = QtWidgets.QLineEdit()
                lbCode.setObjectName("lbCode")
                lbCode.setText(str(code))
                lbCode.setFont(font)
                lbCode.setReadOnly(True)
                lbCode.setStyleSheet("background:rgb(255, 255, 255);color: rgb(0, 0, 0);border: no boder;border-radius: 0px;")
                self.uic.vLayout.addWidget(lbCode)
        def btn_Model_Clicked(self):
                main_win = Main_login()
                main_win.show()
        def Load_list_Model_for_Cbbox(self):
                listmodel= self.controler.fileclass.load_List_Models()
                self.uic.cbListmodel.addItems(listmodel)

        def load_model_infor(self):
                modelname=self.uic.cbListmodel.currentText()
                listmodel= self.controler.fileclass.load_List_Models()
                if modelname not in listmodel:
                        msgstr= modelname +' not exist in list model!'
                        msg = QtWidgets.QMessageBox()
                        msg.setIcon(QtWidgets.QMessageBox.Critical)
                        msg.setText(msgstr)
                        msg.setWindowTitle("Error")
                        msg.exec_()
                        return 0
                else:
                        dict=self.controler.fileclass.load_File(modelname)
                        self.countOfCode=dict['Count']
                        self.totalCountOfProduct=dict['CountOfProduct']
                        self.rects=dict['Rects']
                        self.deep_model =dict['deep']
                        self.ch1= dict['Light']['CH1']
                        self.ch2=dict['Light']['CH2']
                        self.ExposureTime=dict['camera']['ExposureTime']
                        self.Gamma=dict['camera']['Gamma']
                        self.Z_pulse=dict['PLC']['Z_value']
                        self.Y_pulse= dict['PLC']['Y_value']
                        self.path_template=dict['Template']
                        self.Distance_trigger=dict['PLC']['Distance_trigger']
                        return 1
        def get_code_server(self):
                        while True:
                                if self.codeOfBox=='':
                                        codefromserver,check_server=self.controler.get_code_from_server()
                                        if check_server: 
                                                if codefromserver!='':
                                                        self.codeOfBox=codefromserver 
                                                        self.uic.lbcodeOfBox.setText(str(codefromserver))
                                                        break
                                        else:
                                                break
                                box_fail_label,check=self.controler.get_box_fail_from_server() #get result when the label of box fail
                                if box_fail_label!='':
                                        self.codeOfBox=codefromserver 
                                        self.uic.lbcodeOfBox.setText(str(box_fail_label))
                                        self.uic.lbStatus.setText
                                time.sleep(1)

        def thread_get_code_server(self):
                thread=threading.Thread(target=self.get_code_server,args=())
                thread.start()
        def set_icon_btnStart(self,path):
                icon4 = QtGui.QIcon()
                icon4.addPixmap(QtGui.QPixmap(path), QtGui.QIcon.Normal, QtGui.QIcon.Off)
                self.uic.btnStart.setIcon(icon4)
        def main(self):
                checkmodel=self.load_model_infor()
                if checkmodel==1:
                        if self._Run==False:
                                res_plc = self.check_PLC()
                                res_cam=self.check_camera()
                                res_light=self.check_light()
                                server=self.check_server()
                                if  res_cam==1 and res_light==1 and res_plc==1:
                                        self.plc.sendCoordiante(self.Y_pulse,self.Z_pulse,self.Distance_trigger)
                                        self.plc.send_count_product(self.totalCountOfProduct)
                                        self.thread_get_code_server()
                                        self.timer_run()
                                        self.uic.cbListmodel.setEnabled(False)
                                        self.set_icon_btnStart('./Icons/stop.png')
                                        self._Run= True
                        elif self._Run:
                                close = QMessageBox()
                                close.setText("Do you want to STOP?")
                                close.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
                                close = close.exec()
                                if close == QMessageBox.Yes:
                                        self.timer.stop()
                                        self.uic.cbListmodel.setEnabled(True)
                                        self.set_icon_btnStart("./Icons/start.png")
                                        self._Run= False
        def stop_main(self):
                if self._Run==True:
                        self.main()
        def timer_run(self):
                self.template=cv2.imread(self.path_template,1)
                self.timer.timeout.connect(self.run)
                self.timer.start(100)
        def run(self):
                # try:
                        startgrap=self.plc.get_trigger_startgrapbbing()
                        if startgrap==1:
                                self.plc.reset_trigger_startgrapbbing() 
                                if self.count_product==0:
                                        self.isGrab = self.camera.StartGrabbing()
                                self.light.set_light12(int(self.ch1),int(self.ch2))
                        check_triger=self.plc.get_trigger_capture_image()
                        if check_triger ==1 and self.isGrab==True:
                                self.count_product+=1
                                print('count:',self.count_product)
                               
                                self.plc.reset_trigger_capture_image()
                                self.light.Turn_On_Light12()
                                img=self.capture_Image()
                                self.isGrab==False
                                print(' Triggercaptureimage:',check_triger)
                                
                                if self.count_product==self.totalCountOfProduct:
                                        self.plc.set_go_up_after_capture()
                                        self.count_product=0
                                        self.camera.StopGrabbing()
                                self.light.Turn_Of_Light12()
                                self.clear_image()
                                if img.shape[0]!=0:
                                        time_capture=time.time()
                                        modelname=self.uic.cbListmodel.currentText()
                                        img_aligned,image,listcode=self.controler.read_Code_main(img,self.template,self.rects)
                                       
                                        self.set_Image(img)
                                        if len(listcode)==0:
                                                self.plc.setFail()
                                                self.uic.lbStatus.setText('Fail')
                                                self.uic.lbStatus.setStyleSheet("background:rgb(255, 0, 0)")
                                                self.add_label_code('Not decoder!')
                                                self.countFail+=1
                                        else:   
                                                if len(listcode)==self.countOfCode:
                                                # if self.codeOfBox!='':
                                                        # if self.codeOfBox in listcode:
                                                                self.plc.setPass()
                                                                self.save_image_align(img_aligned,modelname,'Pass')
                                                                self.listcode=listcode 
                                                                self.uic.lbStatus.setText('Pass')
                                                                self.uic.lbStatus.setStyleSheet("background:rgb(0, 255, 0)")
                                                                self.countPass+=1
                                                        # else:
                                                        #         self.save_image_align(img_aligned,modelname,'Fail')
                                                        #         self.plc.setFail()
                                                        #         self.uic.lbStatus.setText('Fail')
                                                        #         self.uic.lbStatus.setStyleSheet("background:rgb(255, 0, 0)")
                                                        #         self.countFail+=1
                                                        # self.codeOfBox=''
                                                else:
                                                        self.save_image_align(img_aligned,modelname,'Fail')
                                                        self.plc.setFail()
                                                        self.uic.lbStatus.setText('Fail')
                                                        self.uic.lbStatus.setStyleSheet("background:rgb(255, 0, 0)")
                                                        self.countFail+=1
                                                for i in listcode:
                                                        self.add_label_code(str(i))
                                        self.total+=1
                                        self.show_count()
                                        self.uic.lbcodeOfBox.clear()
                                        
                                else:
                                        res_plc = self.check_PLC()
                                        res_cam=self.check_camera()
                                        res_light=self.check_light()
                                        server=self.check_server()
                                        msg = QtWidgets.QMessageBox()
                                        msg.setIcon(QtWidgets.QMessageBox.Critical)
                                        msg.setText("No capture image!")
                                        msg.setWindowTitle("Error")
                                        msg.exec_()
                                        self.close_control()
                                time_capture1=time.time()
                                TIMEE=time_capture1-time_capture
                                print(TIMEE)
                        get_emergency_stop=self.plc.get_emergency_stop()
                        if get_emergency_stop==1:
                                self.reset()
                                self.uic.lbStatus.setStyleSheet("background:rgb(255, 255, 0)")
                                self.uic.lbStatus.setText('emergency stop')
                        stop_line=self.plc.get_stop_line()
                        if  stop_line==1:
                                self.uic.lbStatus.setStyleSheet("background:rgb(255, 255, 0)")
                                self.uic.lbStatus.setText('Waiting box...')
                        # elif check_triger ==1 and self.isGrab==False:
                        #         self.close_control()
                        #         self._Run=True
                        #         # self.main()
                        #         msg = QtWidgets.QMessageBox()
                        #         msg.setIcon(QtWidgets.QMessageBox.Critical)
                        #         msg.setText("not start grapbing!")
                        #         msg.setWindowTitle("Error")
                        #         msg.exec_()
                
        def capture_Image(self):
                time.sleep(0.2)
                img=np.array([])
                ret = self.camera.GetFrameData()
                if ret == 0:
                        img = self.camera.GetImageArray()
                        img=cv2.cvtColor(img,cv2.COLOR_GRAY2RGB)
                return img

        def set_Image(self,img):
                # img=cv2.cvtColor(img,cv2.COLOR_GRAY2RGB)
                h, w, ch = img.shape
                bytes_per_line = ch * w
                convert_to_Qt_format = QtGui.QImage(img.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
                pixmap= QtGui.QPixmap.fromImage(convert_to_Qt_format)
                self.imageview.setImage(pixmap)

        def clear_image(self):
                self.clear_code() 
                self.uic.lbStatus.setText('Waiting...')
                self.uic.lbStatus.setStyleSheet("background:rgb(255, 255, 0)")

        def save_image_align(self,img,modelname,status):
                timenow=datetime.now()
                parent_dir = "/home/"
                path1 = os.path.join(parent_dir,"pi")
                if not os.path.exists(path1):
                        os.mkdir(path1)
                path2 = os.path.join(path1,'ImageAlign')
                if not os.path.exists(path2):
                        os.mkdir(path2)
                path3 = os.path.join(path2,modelname)
                if not os.path.exists(path3):
                        os.mkdir(path3)
                path4 = os.path.join(path3,status)
                if not os.path.exists(path4):
                        os.mkdir(path4)
                cv2.imwrite(path4+'/'+str(timenow)+'.jpg',img)
        def show_count(self):
                self.uic.lbPass.setText(str(self.countPass))
                self.uic.lbFail.setText(str(self.countFail))
                self.uic.lbYeildrate.setText(str(round((self.countPass/self.total*100),2)))
        def check_PLC(self,Message=True):
                try:
                    check=self.plc.check_plc()
                    if(check==1):
                        self.uic.lbPCL.setText("Connected")
                        self.uic.lbPCL.setStyleSheet("background:rgb(85, 170, 0);color: rgb(255, 255, 255);")
                        return 1
                    else :
                        self.uic.lbPCL.setText("Disconnected")
                        self.uic.lbPCL.setStyleSheet("background:rgba(255, 0, 0, 247);color: rgb(255, 255, 255);")
                        if Message:
                                msg = QtWidgets.QMessageBox()
                                msg.setIcon(QtWidgets.QMessageBox.Critical)
                                msg.setText("Can not connect to PLC!")
                                msg.setWindowTitle("Error")
                                msg.exec_()
                        return -1
                except  Exception as ex:
                        if Message:
                                msg = QtWidgets.QMessageBox()
                                msg.setIcon(QtWidgets.QMessageBox.Critical)
                                msg.setText(ex)
                                msg.setWindowTitle("Error")
                                msg.exec_()
                        return -1 
        def check_server(self,Message=True):
                _,checkserver=self.controler.get_code_from_server()
                if checkserver:
                        self.uic.lbServer.setText("Connected")
                        self.uic.lbServer.setStyleSheet("background:rgb(85, 170, 0);color: rgb(255, 255, 255);")
                        return 1
                else:
                        self.uic.lbServer.setText("Disconnected")
                        self.uic.lbServer.setStyleSheet("background:rgba(255, 0, 0, 247);color: rgb(255, 255, 255);")
                        if Message:
                                msg = QtWidgets.QMessageBox()
                                msg.setIcon(QtWidgets.QMessageBox.Critical)
                                msg.setText("Can not connect to server!")
                                msg.setWindowTitle("Error")
                                msg.exec_()
                        return -1
 ################# Camera ###########################
        
        def check_camera(self,Message=True):
                try:
                    name,devic=list(self.camera.GetDevicesList())
                    if len(devic)>0:
                        camopen=self.camera.OpenCamera(devic[0])
                        if camopen:
                                self.uic.lbCamera.setStyleSheet("background:rgb(85, 170, 0);color: rgb(255, 255, 255);")
                                self.uic.lbCamera.setText("Connected")
                                self.camera.SetParameter(KeyName.ExposureTime,'IFloat',float(self.ExposureTime))
                                self.camera.SetParameter(KeyName.Gamma,'IFloat',float(self.Gamma))
                                rs=self.camera.SetParameter(KeyName.GevSCPD,'IInteger',2000)
                                rs1=self.camera.SetParameter(KeyName.PixelFormat,'IEnumeration',0x01080001)
                                return 1
                        else:
                                self.uic.lbCamera.setText("Disconnected")
                                self.uic.lbCamera.setStyleSheet("background:rgba(255, 0, 0, 247);color: rgb(255, 255, 255);")
                                if Message:
                                        msg = QtWidgets.QMessageBox()
                                        msg.setIcon(QtWidgets.QMessageBox.Critical)
                                        msg.setText("Can not connect to camera!")
                                        msg.setWindowTitle("Error")
                                        msg.exec_()
                                return -1 
                    else:
                        self.uic.lbCamera.setText("Disconnected")
                        self.uic.lbCamera.setStyleSheet("background:rgba(255, 0, 0, 247);color: rgb(255, 255, 255);")
                        if Message:
                                msg = QtWidgets.QMessageBox()
                                msg.setIcon(QtWidgets.QMessageBox.Critical)
                                msg.setText("Not find device to connect to camera!")
                                msg.setWindowTitle("Error")
                                msg.exec_()
                        return -1 
                except  Exception as ex:
                        if Message:
                                msg = QtWidgets.QMessageBox()
                                msg.setIcon(QtWidgets.QMessageBox.Critical)
                                msg.setText(ex)
                                msg.setWindowTitle("Error")
                                msg.exec_()
                        return -1 
 
 ################# Light ##########################
        def check_light(self,Message=True):
                try:
                        rs=self.light.connect()
                        if rs:  
                                
                                self.uic.lbLinght.setStyleSheet("background:rgb(85, 170, 0);color: rgb(255, 255, 255);")
                                self.uic.lbLinght.setText("Connected")
                                return 1
                                
                        else: 
                                self.uic.lbLinght.setStyleSheet("background:rgba(255, 0, 0, 247);color: rgb(255, 255, 255);")
                                self.uic.lbLinght.setText("Disconnected")
                                if Message:
                                        msg = QtWidgets.QMessageBox()
                                        msg.setIcon(QtWidgets.QMessageBox.Critical)
                                        msg.setText("Can not connect to Light!")
                                        msg.setWindowTitle("Error")
                                        msg.exec_()
                                return -1
                except  Exception as ex:
                        msg = QtWidgets.QMessageBox()
                        msg.setIcon(QtWidgets.QMessageBox.Critical)
                        msg.setText(ex)
                        msg.setWindowTitle("Error")
                        msg.exec_()
                        return -1 
        def show(self):
                self.main_win.showMaximized()
def main():
        app = QApplication(sys.argv)
        main_win = MainWindow()
        try: 
                main_win.show()   
                sys.exit(app.exec())
        except Exception as ex: 
                print(ex)                
        finally:
                main_win.close_control()
                         
if __name__ == "__main__":
        main()

    
