import sys
import os
from PyQt5 import QtGui
from PyQt5 import QtWidgets
from numpy.core.fromnumeric import around

sys.path.insert(0,os.path.realpath('./Controller'))
sys.path.insert(0,os.path.realpath('./Views'))
sys.path.insert(0,os.path.realpath('./Lib'))
sys.path.insert(0,os.path.realpath('./Files'))

from PyQt5.QtCore import QTimer, Qt
from PyQt5.QtGui import QPixmap
import cv2   
from HikCamera import KeyName,HikCamera
from parameter import parameterClass
from linkCodeController import ControlerClass
from PyQt5.QtWidgets import QApplication,QDialog, QMessageBox
from UI_Infor import Ui_ModelManager
from UI_setup import Ui_setupClass
import threading 
from Image_viewer import *
from PLCAction import *
from HikCamera import *
from LightAction import *

class Main_setup_model():
    def __init__(self,modelname):
        self.modelname=modelname
        self.parameter=parameterClass()
        self.main_win = QDialog()
        self.imageview=ImageViewerClass(self.main_win)
        self.uic = Ui_setupClass()
        self.uic.setupUi(self.main_win)
        self.uic.vlayout_camview.addWidget(self.imageview)
        self.uic.vlayout_camview.setAlignment(QtCore.Qt.AlignLeft)
        self.main_win.closeEvent=self.closeEvent
        self.z_value_go=0
        self.y_value_go=0
        self.stop_camera=True
        self.captureimage=False
        self.pathToImgage=''
        self.rects=[]
        self.model_crtl=ControlerClass()
        self.load_model_setting()

        self.plc=PLC_Action_Class(self.parameter.plc_ip,self.parameter.plc_port)
        self.light=LightAction(self.parameter.light_portname)
        self.camera=HikCamera()
        self.event()
        
    def event(self):
        self.set_light()
        self.set_camera()
        self.set_PLC_control() 
        self.uic.spbDeep.editingFinished.connect(self.go_y_z)
        self.uic.btnsave.clicked.connect(self.save_File)
        self.uic.btnDecode.clicked.connect(self.btnDecodeClicked)
    def login_plc(self): 
            self.plc.setup_bit_ON()
    def logOut_plc(self): 
            self.plc.setup_bit_OFF()
    def check_PLC(self):
        try:
            check=self.plc.check_plc()
            if check==1:
                self.login_plc()
                return 1
            else : 
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Critical)
                msg.setText("Not connect to PLC!")
                msg.setWindowTitle("Error")
                msg.exec_()
                return -1
        except  Exception as ex:
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Critical)
                msg.setText(ex)
                msg.setWindowTitle("Error")
                msg.exec_()
 ################# Camera ###########################
    def check_camera(self):
                try:  
                    name,devic=list(self.camera.GetDevicesList())
                    if len(devic)>0:
                        camopen=self.camera.OpenCamera(devic[0])
                        if camopen:
                            return 1
                        else:
                                self.uic.lbCamera.setText("Disconnected")
                                self.uic.lbCamera.setStyleSheet("background:rgba(255, 0, 0, 247);color: rgb(255, 255, 255);")
                                msg = QtWidgets.QMessageBox()
                                msg.setIcon(QtWidgets.QMessageBox.Critical)
                                msg.setText("Disonnect to camera!")
                                msg.setWindowTitle("Error")
                                msg.exec_()
                                return -1 
                    else:
                        msg = QtWidgets.QMessageBox()
                        msg.setIcon(QtWidgets.QMessageBox.Critical)
                        msg.setText("Not find device to connect to camera!")
                        msg.setWindowTitle("Error")
                        msg.exec_()
                        return -1 
                                

                except  Exception as ex:
                        msg = QtWidgets.QMessageBox()
                        msg.setIcon(QtWidgets.QMessageBox.Critical)
                        msg.setText(ex)
                        msg.setWindowTitle("Error")
                        msg.exec_()
                        return -1 
 
 ################# Light ##########################
    def check_light(self):
                try: 
                        rs=self.light.connect()
                        if rs: 
                            self.light.open()
                            return 1 
                        else: 
                            msg = QtWidgets.QMessageBox()
                            msg.setIcon(QtWidgets.QMessageBox.Critical)
                            msg.setText("Not connect to Light!")
                            msg.setWindowTitle("Error")
                            msg.exec_()
                            return -1
                except  Exception as ex:
                        msg = QtWidgets.QMessageBox()
                        msg.setIcon(QtWidgets.QMessageBox.Critical)
                        msg.setText(ex)
                        msg.setWindowTitle("Error")
                        msg.exec_()
                        return -1 
    def set_light(self):
        rs=self.check_light()
        if rs==1: 
            self.light.Turn_On_Light12()
            self.uic.spbCH1.valueChanged.connect(lambda:self.setlight1())
            self.uic.spbCH2.valueChanged.connect(lambda:self.setlight2())
         
    def set_camera(self):
            rs=self.check_camera()
            if rs ==1:
                self.uic.spbExposureTime.valueChanged.connect(lambda:self.setExposureTime())
                self.uic.spbGamma.valueChanged.connect(lambda:self.setGamma())
                self.camera.SetParameter(KeyName.ExposureTime,'IFloat',float(self.uic.spbExposureTime.value()))
                self.camera.SetParameter(KeyName.Gamma,'IFloat',float(self.uic.spbGamma.value()))
                rs=self.camera.SetParameter(KeyName.GevSCPD,'IInteger',2000)
                self.camera.SetParameter(KeyName.PixelFormat,'IEnumeration',0x01080001)
                self.camera.StartGrabbing() 
                self.qtimer_camera()
    def go_y_z(self):
        rs= self.check_PLC()
        if rs==1:
            deep=self.uic.spbDeep.value()
            if deep >=10 and deep < self.parameter.originalDistance:
                Z_value= int((self.parameter.originalDistance-self.parameter.workingDistance-deep)*self.parameter.averagePulse)
                self.plc.go_Y_Z(self.y_value_go,Z_value)
    def set_PLC_control(self):
        rs= self.check_PLC()
        if rs==1: 
            self.uic.btnpulse_Y_go_in.pressed.connect(self.plc.set_Y_go_in_ON)
            self.uic.btnpulse_Y_go_in.released.connect(self.plc.set_Y_go_in_OFF)
            self.uic.btnPulse_Y_go_out.pressed.connect(self.plc.set_Y_go_out_ON)
            self.uic.btnPulse_Y_go_out.released.connect(self.plc.set_Y_go_out_OFF)
            self.uic.btnPulse_Z_go_up.pressed.connect(self.plc.set_Z_go_up_ON)
            self.uic.btnPulse_Z_go_up.released.connect(self.plc.set_Z_go_up_OFF)
            self.uic.btnPulse_Z_go_Down.pressed.connect(self.plc.set_Z_go_down_ON)
            self.uic.btnPulse_Z_go_Down.released.connect(self.plc.set_Z_go_down_OFF)
            self.go_y_z()
         
    # def set_Z_go_down(self):
        # deep=self.uic.spbDeep.value()
        # z_value_current=(parameter.originalDistance-parameter.workingDistance-deep)*parameter.averagePulse
        # # getZvalue=self.plc.get_Z_read()
        # getZvalue=0
        # while getZvalue<=z_value_current:
            #    getZvalue= self.plc.set_Z_go_down_ON()
    
    def load_model_setting(self):
            self.uic.lbmodelname.setText(self.modelname)
            dict=self.model_crtl.fileclass.load_File(self.modelname)
            self.uic.spbDeep.setValue(int(dict['deep']))
            self.uic.spbCH1.setValue(float(dict['Light']['CH1']))
            self.uic.spbCH2.setValue(float(dict['Light']['CH2']))
            self.uic.spbExposureTime.setValue(float(dict['camera']['ExposureTime']))
            self.uic.spbGamma.setValue(float(dict['camera']['Gamma']))
            self.uic.spbCountOfCode.setValue(int(dict['Count']))
            self.uic.spbCountOfProduct.setValue(int(dict['CountOfProduct']))
            # self.z_value_go=dict['PLC']['Z_value']
            self.y_value_go=dict['PLC']['Y_value']
#  set camera views
    def qtimer_camera(self):  
            timer=QTimer(self.main_win)
            timer.timeout.connect(self.view_camera)
            timer.start(100)

    def view_camera(self):
            ret = self.camera.GetFrameData()
            if ret == 0:
                img = self.camera.GetImageArray()
                rgb_image=cv2.cvtColor(img,cv2.COLOR_GRAY2RGB)
                img_save=rgb_image.copy()
                parent_dir = self.parameter.path_to_models
                self.pathToImgage=os.path.join(parent_dir,self.modelname+"/"+self.modelname+".jpg")
                if len(self.rects)>0:
                    for i in self.rects:
                        cv2.rectangle(rgb_image,i[0],i[1] , color=(0, 255, 0), thickness=10)

                if self.captureimage:
                    cv2.imwrite(self.pathToImgage,img_save)
                    listcode,image_retan,rects=self.model_crtl.read_Code(img_save)
                    self.rects=rects
                    if len(listcode)>0:
                        for i in listcode:
                            self.add_label_code(str(i))
                    else:
                        self.add_label_code("Not decoder!")
                    self.captureimage = False
                pixmap=self.convert_rgb_to_pixmap(rgb_image)
                self.imageview.setImage(pixmap)
    def convert_rgb_to_pixmap(self,rgb_image):
        h, w, ch = rgb_image.shape
        bytes_per_line = ch * w
        convert_to_Qt_format = QtGui.QImage(rgb_image.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
        pixmap= QPixmap.fromImage(convert_to_Qt_format)
        return pixmap

    def clear_code(self):
            for i in reversed(range(self.uic.vlayout_result.count())): 
                        self.uic.vlayout_result.itemAt(i).widget().deleteLater()
    def add_label_code(self,code):
        lbCode_temp = QtWidgets.QLabel()
        font = QtGui.QFont()
        font.setPointSize(13)
        font.setBold(True)
        font.setWeight(75)
        lbCode_temp.setFont(font)
        lbCode_temp.setText(str(code))
        self.uic.vlayout_result.addWidget(lbCode_temp)
# Set linght
    def setlight1(self):
            self.light.setOne(1,int(self.uic.spbCH1.value()))
    def setlight2(self):
            self.light.setOne(2,int(self.uic.spbCH2.value()))
    def setExposureTime(self):
            exposureTimeValue=float(self.uic.spbExposureTime.value())
            self.camera.SetParameter(KeyName.ExposureTime,'IFloat',exposureTimeValue)
    def setGamma(self):
            gammaValue=float(self.uic.spbGamma.value())
            self.camera.SetParameter(KeyName.Gamma.replace(' ',''),'IFloat',gammaValue)
# save file
    def save_File(self):
        
            # self.clear_code()
            # self.captureimage=True
            nameModel=self.modelname
            countOfProduct=int(self.uic.spbCountOfProduct.value())
            countOfCode=int(self.uic.spbCountOfCode.value())
            template= self.pathToImgage
            deep=int(self.uic.spbDeep.value())
            rects=self.rects
            ch1=self.uic.spbCH1.value()
            ch2=self.uic.spbCH2.value()
            exposureTime=self.uic.spbExposureTime.value()
            gamma=self.uic.spbGamma.value()
            Z_value= (self.parameter.originalDistance-self.parameter.workingDistance-deep)*self.parameter.averagePulse
            Y_value,_,Distange_triger=self.plc.get_PLC_pulse()
            
            rs=self.model_crtl.fileclass.save_File(nameModel,countOfCode,countOfProduct,rects,template,deep,ch1,ch2,exposureTime,gamma,Z_value,Y_value,Distange_triger)
            if rs==1:
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Information)
                msg.setText("Save succesfully!")
                msg.setWindowTitle("infor")
                msg.exec_()
       
    def closeEvent(self, event):
        close = QMessageBox()
        close.setWindowTitle("Information")
        close.setText( "Are you sure want to exit?")
        close.setStandardButtons(QMessageBox.No|QMessageBox.Yes )
        close = close.exec()
        if close == QMessageBox.Yes:
            self.close_control()
            event.accept()
        else:
            event.ignore()
    def close_control(self):
        self.camera.StopGrabbing()
        self.camera.StopCamera()
        self.light.Turn_Of_Light12()
        self.light.close()
        self.logOut_plc()
    def show(self):
        try:
            self.main_win.exec()
        except:
            self.close_control()
    def btnDecodeClicked(self):
        self.captureimage=True
        self.clear_code()
if __name__ == "__main__":
    app = QApplication(sys.argv)
    main_win = Main_setup_model('c')
    main_win.show()
    sys.exit(app.exec())
