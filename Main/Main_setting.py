import sys
import os
from PyQt5 import QtGui, QtWidgets

sys.path.insert(0,os.path.realpath('./Controller'))
sys.path.insert(0,os.path.realpath('./Views'))
sys.path.insert(0,os.path.realpath('./Lib'))
sys.path.insert(0,os.path.realpath('./Files'))

from Main_infor import Main_model_infor
from linkCodeController import ControlerClass
from PyQt5.QtWidgets import QApplication,QDialog, QMessageBox
from UI_setting import Ui_Setting_control
from parameter import *
from LightAction import LightAction
from PLCAction import  *
from Client_Http import *
class Main_setting():
    def __init__(self):
        self.dialog = QDialog()
        self.uic = Ui_Setting_control()
        self.uic.setupUi(self.dialog)
        self._parameter=parameterClass()
        self.light=LightAction(self._parameter.light_portname)
        self.plc=PLC_Action_Class(self._parameter.plc_ip,self._parameter.plc_port)
        self.server=Http_client_Class(self._parameter.serverIP,self._parameter.server_Port)
        self.event()
    def event(self):
        self.uic.cblightPort.setEditable(True)
        self.uic.cblightPort.completer().setCompletionMode(QtWidgets.QCompleter.PopupCompletion)
        # self.uic.cbListModel.setInsertPolicy(QComboBox.NoInsert)
        self.load()
        self.uic.btnsave.clicked.connect(self.save)
        self.uic.btnconnectLight.clicked.connect(self.light_connect)
        self.uic.btnconnect_server.clicked.connect(self.server_connect)
        self.uic.btnconnectPLC.clicked.connect(self.plc_connect)
    def load(self):
        light_portname,plc_ip,plc_port,server_IP,server_Port,workingDistance=self._parameter.load_parameter()
        self.uic.cblightPort.addItem(light_portname)
        self.uic.le_pcl_ip.setText(plc_ip)
        self.uic.le_plc_port.setText(str(plc_port))
        self.uic.le_server_ip.setText(server_IP)
        self.uic.le_server_port.setText(str(server_Port))
        self.uic.le_working_distance.setText(str(workingDistance))
        list_port=self.light.getport()
        for i in list_port:
            if i !=light_portname:
                self.uic.cblightPort.addItem(i)
    def save(self):
            light_portname=self.uic.cblightPort.currentText()
            plc_ip=self.uic.le_pcl_ip.text()
            plc_port=int(self.uic.le_plc_port.text())
            server_IP=self.uic.le_server_ip.text()
            server_Port=int(self.uic.le_server_port.text())
            workingDistance=float(self.uic.le_working_distance.text())
            rs=self._parameter.save_parameter(light_portname,plc_ip,plc_port,server_IP,server_Port,workingDistance)
            if rs==1:
                msgstr= "Save succesfully."
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Critical)
                msg.setText(msgstr)
                msg.setWindowTitle("Error")
                msg.exec_()
            else:
                msgstr= "Can't  save!"
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Critical)
                msg.setText(msgstr)
                msg.setWindowTitle("Error")
                msg.exec_()

    def light_connect(self):
        portname=self.uic.cblightPort.currentText()
        self.light=LightAction(portname)
        isConnect=self.light.connect()
        print(isConnect)
        if isConnect==True:
            self.uic.lbcheckLight.setPixmap(QtGui.QPixmap("./Icons/green-checkbox-png-7.png"))
            self.uic.lbcheckLight.setScaledContents(True)
        else:
            self.uic.lbcheckLight.setPixmap(QtGui.QPixmap("./Icons/remove.png"))
            self.uic.lbcheckLight.setScaledContents(True)
    
    def plc_connect(self):
        plc_ip=self.uic.le_pcl_ip.text()
        plc_port=int(self.uic.le_plc_port.text())
        plc=PLC_Action_Class(plc_ip,plc_port)
        isConnect=plc.check_plc()
        if isConnect==1:
            self.uic.lbCheckPLC.setPixmap(QtGui.QPixmap("./Icons/green-checkbox-png-7.png"))
            self.uic.lbCheckPLC.setScaledContents(True)
        else:
            self.uic.lbCheckPLC.setPixmap(QtGui.QPixmap("./Icons/remove.png"))
            self.uic.lbCheckPLC.setScaledContents(True)
    def server_connect(self):
        server_IP=self.uic.le_server_ip.text()
        server_Port=int(self.uic.le_server_port.text())
        client=Http_client_Class(server_IP,server_Port)
        _,isConnect=client.get('users')

        if isConnect==1:
            self.uic.lbCheck_server.setPixmap(QtGui.QPixmap("./Icons/green-checkbox-png-7.png"))
            self.uic.lbCheck_server.setScaledContents(True)
        else:
            self.uic.lbCheck_server.setPixmap(QtGui.QPixmap("./Icons/remove.png"))
            self.uic.lbCheck_server.setScaledContents(True)

    def show(self):
        self.dialog.exec()
    

if __name__ == "__main__":
    app = QApplication(sys.argv)
    dialog = Main_setting()
    dialog.show()
    sys.exit(app.exec())
  