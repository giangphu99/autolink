import sys
import os
from PyQt5 import QtGui
from PyQt5 import QtWidgets
import cv2 as cv
sys.path.insert(0,os.path.realpath(r'./Controller'))
sys.path.insert(0,os.path.realpath(r'./Views'))
sys.path.insert(0,os.path.realpath(r'./Lib'))
sys.path.insert(0,os.path.realpath(r'./Files'))
from linkCodeController import ControlerClass
from PyQt5.QtWidgets import QApplication, QComboBox,QDialog, QMainWindow, QMessageBox
from UI_Infor import Ui_ModelManager
from Main_setup import Main_setup_model
from UI_template_view import Ui_template_view_class
from Image_viewer import ImageViewerClass 
class Main_model_infor():
    def __init__(self):
        self.main_win = QDialog()
        self.uic = Ui_ModelManager()
        self.uic.setupUi(self.main_win)
        self.uic.cbListModel.setEditable(True)
        self.uic.cbListModel.completer().setCompletionMode(QtWidgets.QCompleter.PopupCompletion)
        self.uic.cbListModel.setInsertPolicy(QComboBox.NoInsert)
        self.model_crtl=ControlerClass()
        # self.main_win.closeEvent=self.closeEvent
        self.pixmap_template=None
        self.event()
    def event(self):
        self.load_model_to_cbbox()
        self.load_model_infor()
        self.uic.cbListModel.activated.connect(self.load_model_infor)
        self.uic.btnSetUpControl.clicked.connect(self.show_mosel_setting)
        # self.uic.lbtemplate.mouseDoubleClickEvent=self.lbtemplate_mouseDoubleClickEvent
        self.uic.btnReLoadModel.clicked.connect(self.btnLoadClicked)
        self.uic.btnview_template.clicked.connect(self.show_template)
    def btnLoadClicked(self):
        self.uic.cbListModel.clear()
        self.load_model_to_cbbox()
        self.load_model_infor()
     
    def load_model_infor(self):
        nameModel=self.uic.cbListModel.currentText()
        dict=self.model_crtl.fileclass.load_File(nameModel)
        if dict==-1:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Information)
            msg.setText("Can't open file!")
            msg.setWindowTitle("Error")
            msg.exec_()
        else:
            self.uic.lbModelName.setText(nameModel)
            self.uic.lbmodeDeep.setText(str(dict['deep']))
            self.uic.lbCountOfProduct.setText(str(dict['CountOfProduct']))
            self.uic.lbModifile.setText(str(dict['modifiled']))
            self.uic.lbcreate.setText(str(dict['created']))
            self.uic.lbCH1.setText(str(dict['Light']['CH1']))
            self.uic.lbCH2.setText(str(dict['Light']['CH2']))
            self.uic.lbExposurettime.setText(str(dict['camera']['ExposureTime']))
            self.uic.lbgamma.setText(str(dict['camera']['Gamma']))  
            self.uic.lbCountOfcode.setText(str(dict['Count']))
            self.uic.lbYvalue.setText(str(dict['PLC']['Y_value']))
            self.uic.lbZvalue.setText(str(dict['PLC']['Z_value']))
            self.uic.lbDistancetrigger.setText(str(dict['PLC']['Distance_trigger']))
            template=dict['Template']
            # if template!='':
            self.set_template_image(template)
    def load_model_to_cbbox(self):
        listModel=self.model_crtl.get_models_from_server()
        if  len(listModel)==0:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setText("Disconnect to server!")
            msg.setWindowTitle("Error")
            msg.exec_()
        else:
            self.model_crtl.fileclass.create_model(listModel)
        listModels=self.model_crtl.fileclass.load_List_Models()
        listModels.sort()
        self.uic.cbListModel.addItems(listModels)
    def show(self):
        self.main_win.exec()
    def show_mosel_setting(self):
        listModels=self.model_crtl.fileclass.load_List_Models()
        modelname=self.uic.cbListModel.currentText()
        if modelname not in listModels:
            mString=modelname+" not exist in list models!"
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setText(mString)
            msg.setWindowTitle("Error")
            msg.exec_()
        else:
            setting = Main_setup_model(modelname)
            setting.show()
    def set_template_image(self,path):
        if path!='':
            template=cv.imread(path) 
            h, w, ch = template.shape
            bytes_per_line = ch * w
            convert_to_Qt_format = QtGui.QImage(template.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
            self.pixmap_template= QtGui.QPixmap.fromImage(convert_to_Qt_format)
            self.uic.lbtemplate.setPixmap(self.pixmap_template)
            self.uic.lbtemplate.setScaledContents(True)
        else:
            self.uic.lbtemplate.clear()

    def show_template(self):
        qdialog1 = QDialog()
        uic1 = Ui_template_view_class()
        uic1.setupUi(qdialog1)
        view=ImageViewerClass(qdialog1)
        uic1.vlayout.addWidget(view)
        view.setImage(self.pixmap_template)
        view.fitInView() 
        qdialog1.exec()
     
    # def closeEvent(self, event): 
    #         close = QMessageBox()
    #         close.setWindowTitle("Information")
    #         close.setText( "Are you sure want to exit?")
    #         close.setStandardButtons(QMessageBox.No|QMessageBox.Yes )
    #         close = close.exec()
    #         if close == QMessageBox.Yes:
    #                 event.accept()
    #         else:
    #                 event.ignore()
    # def get
if __name__ == "__main__":
    app = QApplication(sys.argv)
    main_win = Main_model_infor()
    main_win.show()
    sys.exit(app.exec())
