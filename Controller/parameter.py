import json
import sys
import os
class parameterClass():
    def __init__(self):
        # light
        self.light_portname='/dev/ttyUSB0'
            # PLC1
        self.plc_ip='192.168.1.4'
        self.plc_port=5004
            # PLC2
        # plc_ip='192.168.1.3'
        # plc_port=500
            # Server
        self.serverIP='192.168.1.12'
        self.server_Port=8888
        self.path_to_models='/home/pi/LinkCode/models/'
        self.path_to_setting='/home/pi/LinkCode/settings/settings.json'
        self.pathTo_align='/home/pi/Align'
        self.originalDistance=435
        self.workingDistance=145
        self.averagePulse=378
        self.UrlServer=""
        self.create_folder_setting()
        self.load_parameter()

    def load_file(self):
        try:
            dict={}
            with open(self.path_to_setting, 'r') as f:
                    dict=json.loads(f.read())
            return dict
        except:
            return -1
    def load_parameter(self):
        dict=self.load_file() 
        self.light_portname= dict['Light']['portname'] 
        self.plc_ip=dict['PLC']['ip']
        self.plc_port=dict['PLC']['port']
        self.workingDistance=  dict['Camera']['workingDistance']
        self.serverIP=dict['Server']['ip']
        self.server_Port=dict['Server']['port']
        self.UrlServer="http://"+self.serverIP+":"+str(self.server_Port)+"/"

        return self.light_portname,self.plc_ip,self.plc_port,self.serverIP,self.server_Port,self.workingDistance

    def create_folder_setting(self):
        parent_dir = "/home/"
        path1 = os.path.join(parent_dir,"pi")
        if not os.path.exists(path1):
            os.mkdir(path1)
        path2 = os.path.join(path1,"LinkCode") 
        if not os.path.exists(path2):
            os.mkdir(path2)
        path3 = os.path.join(path2,"settings")
        if not os.path.exists(path3):
            os.mkdir(path3)
        path4 = os.path.join(path3,"settings.json")
        if not os.path.exists(path4):
            self.create_json_setting_control(path4)
        self.path_to_setting=path4
    def create_json_setting_control(self,path):
        # light
        dictlight={}
        dictlight['portname']=self.light_portname
        # plc
        dictPLC={}
        dictPLC['ip'] = self.plc_ip
        dictPLC['port'] = self.plc_port
        # server
        dictServer = {}
        dictServer['ip'] = self.serverIP
        dictServer['port'] = self.server_Port
        # camera
        dictCamera={}
        # dictCamera['originalDistance']=self.originalDistance
        dictCamera['workingDistance']=self.workingDistance
        # dictCamera['averagePulse']=self.averagePulse 
        dict = {}
        dict['Light'] = dictlight
        dict['PLC']=dictPLC
        dict['Camera']=dictCamera
        dict['Server']=dictServer
        with open(path, "w") as f:
            json.dump(dict,f)
    def save_parameter(self,Light_portname,PLC_ip,PLC_port,serverIP,serverPort,camera_workingDistance):
        path=self.path_to_setting
        try:
            with open(path, 'r+') as file:
                dict=json.loads(file.read())
                dict['Light']['portname'] =  Light_portname
                dict['PLC']['ip']= PLC_ip
                dict['PLC']['port']= PLC_port
                dict['Camera']['workingDistance']= camera_workingDistance
                dict['Server']['ip']= serverIP
                dict['Server']['port']= serverPort
                file.seek(0)
                json.dump(dict, file, indent=4)
                file.truncate()
                return 1
        except:
            return-1

