from PIL import ImageTk,Image
import time
import sys
import cv2
import numpy as np
from ctypes import *
global libc
libc=CDLL("libc.so.6")
sys.path.append("/opt/MVS/Samples/armhf/Python/MvImport")
from MvCameraControl_class import *

class HikCamera():
    global deviceList 
    deviceList = MV_CC_DEVICE_INFO_LIST()
    global tlayerType
    tlayerType = MV_GIGE_DEVICE | MV_USB_DEVICE
    data_buf = None
    global cam
    global b_startgrabbing
    b_startgrabbing = False
    b_saveBmp = False
    g_bExit = False
    global b_opendevice
    b_opendevice = False
    cam = MvCamera()
    global hThreadHandle
    hThreadHandle = 0

    @staticmethod
    def SetParameter(key,data_type,value=None):
        if data_type=='IEnumeration':
            value=int(value)
            ret = cam.MV_CC_SetEnumValue(key,value)
            if ret!=0:
                return ret
        elif data_type=='IInteger':
            value=int(value)
            ret = cam.MV_CC_SetIntValue(key,value)
            if ret!=0:
                return ret  

        elif data_type=='IFloat':
            value=float(value)
            ret = cam.MV_CC_SetFloatValue(key,value)
            if ret!=0:
                return ret  

        elif data_type=='IBoolean':
            if value== '0' or value == '1':
                value=int(value)
            ret = cam.MV_CC_SetBoolValue(key,value)
            if ret!=0:
                return ret
        elif data_type=='ICommand':
            ret= cam.MV_CC_SetCommandValue(key)
            if ret!=0:
                return ret
        elif data_type=='IString':
            ret= cam.MV_CC_SetStringValue(key,value)
            if ret!=0:
                return ret
        else:
            print('Wrong data_type')
            return 1  
        return 0

    @staticmethod            
    def GetParameter(key,data_type):
        value=None
        if data_type=='IEnumeration':
            ret = cam.MV_CC_GetEnumValue(key,value)
            if ret!=0:
                return ret,value

        elif data_type=='IInteger':
            ret = cam.MV_CC_GetIntValue(key,value)
            if ret!=0:
                return ret,value

        elif data_type=='IFloat':
            ret = cam.MV_CC_GetFloatValue(key,value)
            if ret!=0:
                return ret,value  

        elif data_type=='IBoolean':
            ret = cam.MV_CC_GetBoolValue(key,value)
            if ret!=0:
                return ret,value

        elif data_type=='IString':
            ret = cam.MV_CC_GetStringValue(key,value)
            if ret!=0:
                return ret,value
        else:
            print('Wrong data_type')
            return 1,value
        return 0,value

    @staticmethod
    def GetDevicesList():
        global deviceList
        deviceList = MV_CC_DEVICE_INFO_LIST()
        tlayerType = MV_GIGE_DEVICE | MV_USB_DEVICE
        ret = MvCamera.MV_CC_EnumDevices(tlayerType, deviceList)
        if ret != 0:
            print ("enum devices fail! ret[0x%x]" % ret)
        
        if deviceList.nDeviceNum == 0:
            print ("find no device!")
            
        model_list=[]
        global SN_list
        SN_list=[]
        for i in range(0, deviceList.nDeviceNum):
            mvcc_dev_info = cast(deviceList.pDeviceInfo[i], POINTER(MV_CC_DEVICE_INFO)).contents
            if mvcc_dev_info.nTLayerType == MV_GIGE_DEVICE:
                strModeName = ""
                for per in mvcc_dev_info.SpecialInfo.stGigEInfo.chModelName:
                    strModeName = strModeName + chr(per)
                model_list.append(strModeName)
                strSerialNumber = ""
                for per in mvcc_dev_info.SpecialInfo.stGigEInfo.chSerialNumber:
                    if per == 0:
                        break
                    strSerialNumber = strSerialNumber + chr(per)

                SN_list.append(strSerialNumber)
                
            elif mvcc_dev_info.nTLayerType == MV_USB_DEVICE:
                strModeName = ""
                for per in mvcc_dev_info.SpecialInfo.stUsb3VInfo.chModelName:
                    if per == 0:
                        break
                    strModeName = strModeName + chr(per)
                model_list.append(strModeName)
                strSerialNumber = ""
                for per in mvcc_dev_info.SpecialInfo.stUsb3VInfo.chSerialNumber:
                    if per == 0:
                        break
                    strSerialNumber = strSerialNumber + chr(per)
                SN_list.append(strSerialNumber)
        return model_list,SN_list
 
    @staticmethod
    def OpenCamera(SerialNum):
        global deviceList
        global nPayloadSize
        global data_buf
        global cam
        global b_opendevice
        global g_bExit
        g_bExit  = False
        global index
        global first
        first=True
        if False == b_opendevice:

            for index,val in enumerate(SN_list):
                if SerialNum==val:
                    index_device=index
                    break
            # ch:选择设备并创建句柄 | en:Select device and create handle
            stDeviceList = cast(deviceList.pDeviceInfo[index_device], POINTER(MV_CC_DEVICE_INFO)).contents

            ret = cam.MV_CC_CreateHandle(stDeviceList)
            if ret != 0:
                print ("create handle fail! ret[0x%x]" % ret)

            # ch:打开设备 | en:Open device
            ret = cam.MV_CC_OpenDevice(MV_ACCESS_Exclusive, 0)
            if ret != 0:
                print ("open device fail! ret[0x%x]" % ret)
                # return ret
                b_opendevice=False
            else: 
                b_opendevice = True
            # ch:探测网络最佳包大小(只对GigE相机有效) | en:Detection network optimal package size(It only works for the GigE camera)
            if stDeviceList.nTLayerType == MV_GIGE_DEVICE:
                nPacketSize = cam.MV_CC_GetOptimalPacketSize()
                if int(nPacketSize) > 0:
                    ret = cam.MV_CC_SetIntValue("GevSCPSPacketSize",nPacketSize)
                    if ret != 0:
                        print ("warning: set packet size fail! ret[0x%x]" % ret)
                else:
                    print ("warning: set packet size fail! ret[0x%x]" % nPacketSize)

            stBool = c_bool(False)
            ret =cam.MV_CC_GetBoolValue("AcquisitionFrameRateEnable", stBool)
            if ret != 0:
                print ("get acquisition frame rate enable fail! ret[0x%x]" % ret)

            stParam =  MVCC_INTVALUE()
            memset(byref(stParam), 0, sizeof(MVCC_INTVALUE))
            
            ret = cam.MV_CC_GetIntValue("PayloadSize", stParam)
            if ret != 0:
                print ("get payload size fail! ret[0x%x]" % ret)
            nPayloadSize = stParam.nCurValue
            data_buf = (c_ubyte * nPayloadSize)()
        return b_opendevice
    # 
    @staticmethod
    def StartGrabbing():
        global b_startgrabbing
        global hThreadHandle
        if False == b_startgrabbing and b_opendevice == True:
        # ch:开始取流 | en:Start grab image
            ret = cam.MV_CC_StartGrabbing()
            
            if ret != 0:
                print ("start grabbing fail! ret[0x%x]" % ret)
            else:
                print("start grab")
                b_startgrabbing = True
        return b_startgrabbing

    @staticmethod
    def StopGrabbing():
        global b_startgrabbing
        global hThreadHandle
        
        if True == b_startgrabbing and b_opendevice == True:
        # ch:开始取流 | en:Start grab image
            ret = cam.MV_CC_StopGrabbing()
            if ret != 0:
                print ("stop grabbing fail! ret[0x%x]" % ret)
            else:
                b_startgrabbing = False
        return  b_startgrabbing


    @staticmethod
    def StopCamera():
        global cam
        global g_bExit
        global b_opendevice
        global b_startgrabbing

        g_bExit = True
        # ch:关闭设备 | Close device
        if True == b_opendevice:
            ret = cam.MV_CC_CloseDevice()
            if ret != 0:
                return ret

        # ch:销毁句柄 | Destroy handle
        ret = cam.MV_CC_DestroyHandle()
        if ret != 0:
            return ret
        b_opendevice = False
        b_startgrabbing = False
        return 0
    @staticmethod
    def SaveFeature():
        ret = cam.MV_CC_FeatureSave("FeatureFile.ini")
        if MV_OK != ret:
            print ("save feature fail! ret [0x%x]" %ret)
    @staticmethod
    def LoadFeature():
        ret = cam.MV_CC_FeatureLoad("FeatureFile.ini")
        if MV_OK != ret:
	        print ("load feature fail! ret [0x%x]" %ret)

    @staticmethod
    def GetFrameData():
        global m_pBufForSaveImage
        global nPayloadSize
        global data_buf
        global cam
        global stFrameInfo
        global first
        global save_stFrameInfo_nWidth
        global save_stFrameInfo_nHeight
        global save_stFrameInfo_enPixelType
        global save_stConvertParam_enPixelType
        global img_buff
        if first:
            save_stFrameInfo_nWidth=0
            m_pBufForSaveImage = None
            stFrameInfo = MV_FRAME_OUT_INFO_EX()
            memset(byref(stFrameInfo), 0, sizeof(stFrameInfo))
            data_buf = (c_ubyte * nPayloadSize)()
            first=False
            img_buff=(c_ubyte * nPayloadSize)()

        global ret
        ret = cam.MV_CC_GetOneFrameTimeout(byref(data_buf), nPayloadSize, stFrameInfo, 1000)
        if ret == 0:
            global stConvertParam
            
            save_stFrameInfo_nWidth=stFrameInfo.nWidth
            save_stFrameInfo_nHeight=stFrameInfo.nHeight
            save_stFrameInfo_enPixelType=stFrameInfo.enPixelType
            #print ("get one frame: Width[%d], Height[%d], nFrameNum[%d]"  % (stFrameInfo.nWidth, stFrameInfo.nHeight, stFrameInfo.nFrameNum))
            #转换像素结构体赋值
            stConvertParam = MV_CC_PIXEL_CONVERT_PARAM()
            memset(byref(stConvertParam), 0, sizeof(stConvertParam))
            stConvertParam.nWidth = stFrameInfo.nWidth
            stConvertParam.nHeight = stFrameInfo.nHeight
            stConvertParam.pSrcData = data_buf
            stConvertParam.nSrcDataLen = stFrameInfo.nFrameLen
            stConvertParam.enSrcPixelType = stFrameInfo.enPixelType 
 
            # Mono8+RGB
            if PixelType_Gvsp_Mono8 == stFrameInfo.enPixelType or PixelType_Gvsp_RGB8_Packed == stFrameInfo.enPixelType:
                libc.memcpy(byref(img_buff), byref(data_buf), nPayloadSize)


            #如果是黑白且非Mono8则转为Mono8
            elif  True == IsMonoData(stFrameInfo.enPixelType):
                nConvertSize = stFrameInfo.nWidth * stFrameInfo.nHeight
                stConvertParam.enDstPixelType = PixelType_Gvsp_Mono8
                stConvertParam.pDstBuffer = (c_ubyte * nConvertSize)()
                stConvertParam.nDstBufferSize = nConvertSize
                ret_mono = cam.MV_CC_ConvertPixelType(stConvertParam)
                if ret_mono != 0:
                    print ("convert Pixel fail[0x%x]" % ret_mono)
                img_buff = (c_ubyte * stConvertParam.nDstLen)()
                libc.memcpy(byref(img_buff), stConvertParam.pDstBuffer, nConvertSize)


            #如果是彩色且非RGB则转为RGB后显示
            elif  True == IsColorData(stFrameInfo.enPixelType):

                nConvertSize = stFrameInfo.nWidth * stFrameInfo.nHeight * 3
                stConvertParam.enDstPixelType = PixelType_Gvsp_RGB8_Packed
                stConvertParam.pDstBuffer = (c_ubyte * nConvertSize)()
                stConvertParam.nDstBufferSize = nConvertSize
                ret_color = cam.MV_CC_ConvertPixelType(stConvertParam)
                if ret_color != 0:
                    print ("convert Pixel fail[0x%x]" % ret_color)

                img_buff = (c_ubyte * stConvertParam.nDstLen)()
                libc.memcpy(byref(img_buff), stConvertParam.pDstBuffer, nConvertSize)
            save_stConvertParam_enPixelType=stConvertParam.enDstPixelType
        return ret
 
    @staticmethod
    def  GetImageFromBytes():
        global img_buff
        global stFrameInfo
        global stConvertParam
        global b_opendevice
        if True == b_opendevice:
            if save_stFrameInfo_nWidth:
                if PixelType_Gvsp_Mono8 == save_stFrameInfo_enPixelType or save_stConvertParam_enPixelType == PixelType_Gvsp_Mono8:
                    image=Image.frombytes('L', (save_stFrameInfo_nWidth, save_stFrameInfo_nHeight), img_buff, 'raw')
                elif PixelType_Gvsp_RGB8_Packed == save_stFrameInfo_enPixelType or save_stConvertParam_enPixelType == PixelType_Gvsp_RGB8_Packed:
                    image=Image.frombytes('RGB', (save_stFrameInfo_nWidth, save_stFrameInfo_nHeight), img_buff, 'raw')
                return image


    @staticmethod    
    def  GetImageArray():
        global img_buff
        global stFrameInfo
        global stConvertParam
        global b_opendevice
        if True == b_opendevice:
            if save_stFrameInfo_nWidth:
                if PixelType_Gvsp_Mono8 == save_stFrameInfo_enPixelType or save_stConvertParam_enPixelType == PixelType_Gvsp_Mono8:
                    numArray = MonoNumPy(img_buff, save_stFrameInfo_nWidth ,save_stFrameInfo_nHeight)
                else:
                    numArray = ColorNumPy(img_buff, save_stFrameInfo_nWidth ,save_stFrameInfo_nHeight)
                return numArray
            return np.array([])
            
            
def IsMonoData(enGvspPixelType):
    if  PixelType_Gvsp_Mono10 == enGvspPixelType or PixelType_Gvsp_Mono10_Packed == enGvspPixelType or PixelType_Gvsp_Mono12 == enGvspPixelType or PixelType_Gvsp_Mono12_Packed == enGvspPixelType:
        return True
    else:
        return False

def IsColorData(enGvspPixelType):
    if PixelType_Gvsp_BayerGR8 == enGvspPixelType or PixelType_Gvsp_BayerRG8 == enGvspPixelType or PixelType_Gvsp_BayerGB8 == enGvspPixelType or PixelType_Gvsp_BayerBG8 == enGvspPixelType or PixelType_Gvsp_BayerGR10 == enGvspPixelType or PixelType_Gvsp_BayerRG10 == enGvspPixelType or PixelType_Gvsp_BayerGB10 == enGvspPixelType or PixelType_Gvsp_BayerBG10 == enGvspPixelType or PixelType_Gvsp_BayerGR12 == enGvspPixelType or PixelType_Gvsp_BayerRG12 == enGvspPixelType or PixelType_Gvsp_BayerGB12 == enGvspPixelType or PixelType_Gvsp_BayerBG12 == enGvspPixelType or PixelType_Gvsp_BayerGR10_Packed == enGvspPixelType or PixelType_Gvsp_BayerRG10_Packed == enGvspPixelType or PixelType_Gvsp_BayerGB10_Packed == enGvspPixelType or PixelType_Gvsp_BayerBG10_Packed == enGvspPixelType or PixelType_Gvsp_BayerGR12_Packed == enGvspPixelType or PixelType_Gvsp_BayerRG12_Packed== enGvspPixelType or PixelType_Gvsp_BayerGB12_Packed == enGvspPixelType or PixelType_Gvsp_BayerBG12_Packed == enGvspPixelType or PixelType_Gvsp_YUV422_Packed == enGvspPixelType or PixelType_Gvsp_YUV422_YUYV_Packed == enGvspPixelType:
        return True
    else:
        return False

def MonoNumPy(data,nWidth,nHeight):
    data_ = np.frombuffer(data, count=int(nWidth * nHeight), dtype=np.uint8, offset=0)
    data_mono_arr = data_.reshape(nHeight, nWidth)
    numArray = np.zeros([nHeight, nWidth],np.uint8) 
    numArray[:, :] = data_mono_arr
    return numArray


def ColorNumPy(data,nWidth,nHeight):
    data_ = np.frombuffer(data, count=int(nWidth*nHeight*3), dtype=np.uint8, offset=0)
    data_r = data_[0:nWidth*nHeight*3:3]
    data_g = data_[1:nWidth*nHeight*3:3]
    data_b = data_[2:nWidth*nHeight*3:3]

    data_r_arr = data_r.reshape(nHeight, nWidth)
    data_g_arr = data_g.reshape(nHeight, nWidth)
    data_b_arr = data_b.reshape(nHeight, nWidth)
    numArray = np.zeros([nHeight, nWidth, 3],"uint8")


    numArray[:, :, 0] = data_r_arr
    numArray[:, :, 1] = data_g_arr
    numArray[:, :, 2] = data_b_arr
    return numArray


class KeyName:
    DeviceType = "DeviceType"
    DeviceScanType = "DeviceScanType"
    DeviceVendorName = "DeviceVendorName"
    DeviceModelName = "DeviceModelName"
    DeviceManufacturerInfo = "DeviceManufacturerInfo"
    DeviceVersion = "DeviceVersion"
    DeviceFirmwareVersion = "DeviceFirmwareVersion"
    DeviceSerialNumber = "DeviceSerialNumber"
    DeviceID = "DeviceID"
    DeviceUserID = "DeviceUserID"
    DeviceUptime = "DeviceUptime"
    BoardDeviceType = "BoardDeviceType"
    DeviceConnectionSelector = "DeviceConnectionSelector"
    DeviceConnectionSpeed = "DeviceConnectionSpeed"
    DeviceConnectionStatus = "DeviceConnectionStatus"
    DeviceLinkSelector = "DeviceLinkSelector"
    DeviceLinkSpeed = "DeviceLinkSpeed"
    DeviceLinkConnectionCount = "DeviceLinkConnectionCount"
    DeviceLinkHeartbeatMode = "DeviceLinkHeartbeatMode"
    DeviceLinkHeartbeatTimeout = "DeviceLinkHeartbeatTimeout"
    DeviceStreamChannelCount = "DeviceStreamChannelCount"
    DeviceStreamChannelSelector = "DeviceStreamChannelSelector"
    DeviceStreamChannelType = "DeviceStreamChannelType"
    DeviceStreamChannelLink = "DeviceStreamChannelLink"
    DeviceStreamChannelEndianness = "DeviceStreamChannelEndianness"
    DeviceStreamChannelPacketSize = "DeviceStreamChannelPacketSize"
    DeviceEventChannelCount = "DeviceEventChannelCount"
    DeviceCharacterSet = "DeviceCharacterSet"
    DeviceReset = "DeviceReset"
    DeviceTemperatureSelector = "DeviceTemperatureSelector"
    DeviceTemperature = "DeviceTemperature"
    FindMe = "FindMe"
    DeviceMaxThroughput = "DeviceMaxThroughput"
    WidthMax  = "WidthMax "
    HeightMax  = "HeightMax "
    RegionSelector  = "RegionSelector "
    RegionDestination = "RegionDestination"
    Width = "Width"
    Height = "Height"
    OffsetX = "OffsetX"
    OffsetY = "OffsetY"
    ReverseX = "ReverseX"
    ReverseY = "ReverseY"
    ReverseScanDirection = "ReverseScanDirection"
    PixelFormat = "PixelFormat"
    PixelSize = "PixelSize"
    ImageCompressionMode = "ImageCompressionMode"
    ImageCompressionQuality = "ImageCompressionQuality"
    TestPatternGeneratorSelector = "TestPatternGeneratorSelector"
    TestPattern = "TestPattern"
    BinningSelector = "BinningSelector"
    BinningHorizontal = "BinningHorizontal"
    BinningVertical = "BinningVertical"
    DecimationHorizontal = "DecimationHorizontal"
    DecimationVertical = "DecimationVertical"
    Deinterlacing = "Deinterlacing"
    FrameSpecInfoSelector = "FrameSpecInfoSelector"
    FrameSpecInfo = "FrameSpecInfo"
    AcquisitionMode = "AcquisitionMode"
    AcquisitionStart = "AcquisitionStart"
    AcquisitionStop = "AcquisitionStop"
    AcquisitionBurstFrameCount  = "AcquisitionBurstFrameCount "
    AcquisitionFrameRate = "AcquisitionFrameRate"
    AcquisitionFrameRateEnable = "AcquisitionFrameRateEnable"
    AcquisitionLineRate = "AcquisitionLineRate"
    AcquisitionLineRateEnable = "AcquisitionLineRateEnable"
    ResultingLineRate = "ResultingLineRate"
    ResultingFrameRate = "ResultingFrameRate"
    TriggerSelector = "TriggerSelector"
    TriggerMode = "TriggerMode"
    TriggerSoftware = "TriggerSoftware"
    TriggerSource = "TriggerSource"
    TriggerActivation = "TriggerActivation"
    TriggerDelay = "TriggerDelay"
    TriggerCacheEnable = "TriggerCacheEnable"
    SensorShutterMode = "SensorShutterMode"
    ExposureMode = "ExposureMode"
    ExposureTime = "ExposureTime"
    ExposureAuto = "ExposureAuto"
    AutoExposureTimeLowerLimit = "AutoExposureTimeLowerLimit"
    AutoExposureTimeUpperLimit = "AutoExposureTimeUpperLimit"
    GainShutPrior = "GainShutPrior"
    FrameTimeoutEnable = "FrameTimeoutEnable"
    FrameTimeoutTime = "FrameTimeoutTime"
    HDREnable = "HDREnable"
    HDRSelector = "HDRSelector"
    HDRShuter = "HDRShuter"
    HDRGain = "HDRGain"
    LineSelector = "LineSelector"
    LineMode = "LineMode"
    LineInverter = "LineInverter"
    LineTermination = "LineTermination"
    LineStatus = "LineStatus"
    LineStatusAll = "LineStatusAll"
    LineSource = "LineSource"
    StrobeEnable = "StrobeEnable"
    LineDebouncerTime = "LineDebouncerTime"
    StrobeLineDuration = "StrobeLineDuration"
    StrobeLineDelay = "StrobeLineDelay"
    StrobeLinePreDelay = "StrobeLinePreDelay"
    CounterSelector = "CounterSelector"
    CounterEventSource = "CounterEventSource"
    CounterResetSource = "CounterResetSource"
    CounterReset = "CounterReset"
    CounterValue = "CounterValue"
    CounterCurrentValue = "CounterCurrentValue"
    Gain = "Gain"
    GainAuto = "GainAuto"
    AutoGainLowerLimit = "AutoGainLowerLimit"
    AutoGainUpperLimit = "AutoGainUpperLimit"
    ADCGainEnable = "ADCGainEnable"
    DigitalShift = "DigitalShift"
    DigitalShiftEnable = "DigitalShiftEnable"
    Brightness = "Brightness"
    BlackLevel = "BlackLevel"
    BlackLevelEnable = "BlackLevelEnable"
    BlackLevelAuto = "BlackLevelAuto"
    BalanceWhiteAuto = "BalanceWhiteAuto"
    BalanceRatioSelector = "BalanceRatioSelector"
    BalanceRatio = "BalanceRatio"
    Gamma = "Gamma"
    GammaSelector = "GammaSelector"
    GammaEnable = "GammaEnable"
    Sharpness = "Sharpness"
    SharpnessEnable = "SharpnessEnable"
    SharpnessAuto = "SharpnessAuto"
    Hue = "Hue"
    HueEnable = "HueEnable"
    HueAuto = "HueAuto"
    Saturation = "Saturation"
    SaturationEnable = "SaturationEnable"
    SaturationAuto = "SaturationAuto"
    DigitalNoiseReductionMode = "DigitalNoiseReductionMode"
    NoiseReduction = "NoiseReduction"
    AirspaceNoiseReduction = "AirspaceNoiseReduction"
    TemporalNoiseReduction = "TemporalNoiseReduction"
    AutoFunctionAOISelector = "AutoFunctionAOISelector"
    AutoFunctionAOIWidth = "AutoFunctionAOIWidth"
    AutoFunctionAOIHeight = "AutoFunctionAOIHeight"
    AutoFunctionAOIOffsetX = "AutoFunctionAOIOffsetX"
    AutoFunctionAOIOffsetY = "AutoFunctionAOIOffsetY"
    AutoFunctionAOIUsageIntensity = "AutoFunctionAOIUsageIntensity"
    AutoFunctionAOIUsageWhiteBalance = "AutoFunctionAOIUsageWhiteBalance"
    LUTSelector = "LUTSelector"
    LUTEnable = "LUTEnable"
    LUTIndex = "LUTIndex"
    LUTValue = "LUTValue"
    LUTValueAll = "LUTValueAll"
    EncoderSelector = "EncoderSelector"
    EncoderSourceA = "EncoderSourceA"
    EncoderSourceB = "EncoderSourceB"
    EncoderTriggerMode = "EncoderTriggerMode"
    EncoderCounterMode = "EncoderCounterMode"
    EncoderCounter = "EncoderCounter"
    EncoderCounterMax = "EncoderCounterMax"
    EncoderCounterReset = "EncoderCounterReset"
    EncoderMaxReverseCounter = "EncoderMaxReverseCounter"
    EncoderReverseCounterReset = "EncoderReverseCounterReset"
    InputSource = "InputSource"
    SignalAlignment = "SignalAlignment"
    PreDivider = "PreDivider"
    Multiplier = "Multiplier"
    PostDivider = "PostDivider"
    ShadingSelector = "ShadingSelector"
    ActivateShading = "ActivateShading"
    NUCEnable = "NUCEnable"
    FPNCEnable = "FPNCEnable"
    PRNUCEnable = "PRNUCEnable"
    UserSetCurrent = "UserSetCurrent"
    UserSetSelector = "UserSetSelector"
    UserSetLoad = "UserSetLoad"
    UserSetSave = "UserSetSave"
    UserSetDefault = "UserSetDefault"
    PayloadSize = "PayloadSize"
    GevVersionMajor = "GevVersionMajor"
    GevVersionMinor = "GevVersionMinor"
    GevDeviceModeIsBigEndian = "GevDeviceModeIsBigEndian"
    GevDeviceModeCharacterSet = "GevDeviceModeCharacterSet"
    GevInterfaceSelector = "GevInterfaceSelector"
    GevMACAddress = "GevMACAddress"
    GevSupportedOptionSelector = "GevSupportedOptionSelector"
    GevSupportedOption = "GevSupportedOption"
    GevCurrentIPConfigurationLLA = "GevCurrentIPConfigurationLLA"
    GevCurrentIPConfigurationDHCP = "GevCurrentIPConfigurationDHCP"
    GevCurrentIPConfigurationPersistentIP = "GevCurrentIPConfigurationPersistentIP"
    GevPAUSEFrameReception = "GevPAUSEFrameReception"
    GevCurrentIPAddress = "GevCurrentIPAddress"
    GevCurrentSubnetMask = "GevCurrentSubnetMask"
    GevCurrentDefaultGateway = "GevCurrentDefaultGateway"
    GevFirstURL = "GevFirstURL"
    GevSecondURL = "GevSecondURL"
    GevNumberOfInterfaces = "GevNumberOfInterfaces"
    GevPersistentIPAddress = "GevPersistentIPAddress"
    GevPersistentSubnetMask = "GevPersistentSubnetMask"
    GevPersistentDefaultGateway = "GevPersistentDefaultGateway"
    GevLinkSpeed = "GevLinkSpeed"
    GevMessageChannelCount = "GevMessageChannelCount"
    GevStreamChannelCount = "GevStreamChannelCount"
    GevHeartbeatTimeout = "GevHeartbeatTimeout"
    GevGVCPHeartbeatDisable = "GevGVCPHeartbeatDisable"
    GevTimestampTickFrequency = "GevTimestampTickFrequency"
    GevTimestampControlLatch = "GevTimestampControlLatch"
    GevTimestampControlReset = "GevTimestampControlReset"
    GevTimestampControlLatchReset = "GevTimestampControlLatchReset"
    GevTimestampValue = "GevTimestampValue"
    GevCCP = "GevCCP"
    GevStreamChannelSelector = "GevStreamChannelSelector"
    GevSCPInterfaceIndex = "GevSCPInterfaceIndex"
    GevSCPHostPort = "GevSCPHostPort"
    GevSCPDirection = "GevSCPDirection"
    GevSCPSFireTestPacket = "GevSCPSFireTestPacket"
    GevSCPSDoNotFragment = "GevSCPSDoNotFragment"
    GevSCPSBigEndian = "GevSCPSBigEndian"
    PacketUnorderSupport = "PacketUnorderSupport"
    GevSCPSPacketSize = "GevSCPSPacketSize"
    GevSCPD = "GevSCPD"
    GevSCDA = "GevSCDA"
    GevSCSP = "GevSCSP"
    TLParamsLocked = "TLParamsLocked"
    typeList = (("DeviceType", "IENUMERATION"),\
                ("DeviceScanType", "IENUMERATION"),\
                ("DeviceVendorName", "ISTRING"),\
                ("DeviceModelName", "ISTRING"),\
                ("DeviceManufacturerInfo", "ISTRING"),\
                ("DeviceVersion", "ISTRING"),\
                ("DeviceFirmwareVersion", "ISTRING"),\
                ("DeviceSerialNumber", "ISTRING"),\
                ("DeviceID", "ISTRING"),\
                ("DeviceUserID", "ISTRING"),\
                ("DeviceUptime", "IINTEGER"),\
                ("BoardDeviceType", "IINTEGER"),\
                ("DeviceConnectionSelector", "IINTEGER"),\
                ("DeviceConnectionSpeed", "IINTEGER"),\
                ("DeviceConnectionStatus", "IENUMERATION"),\
                ("DeviceLinkSelector", "IINTEGER"),\
                ("DeviceLinkSpeed", "IINTEGER"),\
                ("DeviceLinkConnectionCount", "IINTEGER"),\
                ("DeviceLinkHeartbeatMode", "IENUMERATION"),\
                ("DeviceLinkHeartbeatTimeout", "IINTEGER"),\
                ("DeviceStreamChannelCount", "IINTEGER"),\
                ("DeviceStreamChannelSelector", "IINTEGER"),\
                ("DeviceStreamChannelType", "IENUMERATION"),\
                ("DeviceStreamChannelLink", "IINTEGER"),\
                ("DeviceStreamChannelEndianness", "IENUMERATION"),\
                ("DeviceStreamChannelPacketSize", "IINTEGER"),\
                ("DeviceEventChannelCount", "IINTEGER"),\
                ("DeviceCharacterSet", "IENUMERATION"),\
                ("DeviceReset", "ICOMMAND"),\
                ("DeviceTemperatureSelector", "IENUMERATION"),\
                ("DeviceTemperature", "IFLOAT"),\
                ("FindMe", "ICOMMAND"),\
                ("DeviceMaxThroughput", "IINTEGER"),\
                ("WidthMax ", "IINTEGER"),\
                ("HeightMax ", "IINTEGER"),\
                ("RegionSelector ", "IENUMERATION"),\
                ("RegionDestination", "IENUMERATION"),\
                ("Width", "IINTEGER"),\
                ("Height", "IINTEGER"),\
                ("OffsetX", "IINTEGER"),\
                ("OffsetY", "IINTEGER"),\
                ("ReverseX", "IBOOLEAN"),\
                ("ReverseY", "IBOOLEAN"),\
                ("ReverseScanDirection", "IBOOLEAN"),\
                ("PixelFormat", "IENUMERATION"),\
                ("PixelSize", "IENUMERATION"),\
                ("ImageCompressionMode", "IENUMERATION"),\
                ("ImageCompressionQuality", "IINTEGER"),\
                ("TestPatternGeneratorSelector", "IENUMERATION"),\
                ("TestPattern", "IENUMERATION"),\
                ("BinningSelector", "IENUMERATION"),\
                ("BinningHorizontal", "IENUMERATION"),\
                ("BinningVertical", "IENUMERATION"),\
                ("DecimationHorizontal", "IENUMERATION"),\
                ("DecimationVertical", "IENUMERATION"),\
                ("Deinterlacing", "IENUMERATION"),\
                ("FrameSpecInfoSelector", "IENUMERATION"),\
                ("FrameSpecInfo", "IBOOLEAN"),\
                ("AcquisitionMode", "IENUMERATION"),\
                ("AcquisitionStart", "ICOMMAND"),\
                ("AcquisitionStop", "ICOMMAND"),\
                ("AcquisitionBurstFrameCount ", "IINTEGER"),\
                ("AcquisitionFrameRate", "IFLOAT"),\
                ("AcquisitionFrameRateEnable", "IBOOLEAN"),\
                ("AcquisitionLineRate", "IINTEGER"),\
                ("AcquisitionLineRateEnable", "IBOOLEAN"),\
                ("ResultingLineRate", "IINTEGER"),\
                ("ResultingFrameRate", "IFLOAT"),\
                ("TriggerSelector", "IENUMERATION"),\
                ("TriggerMode", "IENUMERATION"),\
                ("TriggerSoftware", "ICOMMAND"),\
                ("TriggerSource", "IENUMERATION"),\
                ("TriggerActivation", "IENUMERATION"),\
                ("TriggerDelay", "IFLOAT"),\
                ("TriggerCacheEnable", "IBOOLEAN"),\
                ("SensorShutterMode", "IENUMERATION"),\
                ("ExposureMode", "IENUMERATION"),\
                ("ExposureTime", "IFLOAT"),\
                ("ExposureAuto", "IENUMERATION"),\
                ("AutoExposureTimeLowerLimit", "IINTEGER"),\
                ("AutoExposureTimeUpperLimit", "IINTEGER"),\
                ("GainShutPrior", "IENUMERATION"),\
                ("FrameTimeoutEnable", "IBOOLEAN"),\
                ("FrameTimeoutTime", "IINTEGER"),\
                ("HDREnable", "IBOOLEAN"),\
                ("HDRSelector", "IINTEGER"),\
                ("HDRShuter", "IINTEGER"),\
                ("HDRGain", "IFLOAT"),\
                ("LineSelector", "IENUMERATION"),\
                ("LineMode", "IENUMERATION"),\
                ("LineInverter", "IBOOLEAN"),\
                ("LineTermination", "IBOOLEAN"),\
                ("LineStatus", "IBOOLEAN"),\
                ("LineStatusAll", "IINTEGER"),\
                ("LineSource", "IENUMERATION"),\
                ("StrobeEnable", "IBOOLEAN"),\
                ("LineDebouncerTime", "IINTEGER"),\
                ("StrobeLineDuration", "IINTEGER"),\
                ("StrobeLineDelay", "IINTEGER"),\
                ("StrobeLinePreDelay", "IINTEGER"),\
                ("CounterSelector", "IENUMERATION"),\
                ("CounterEventSource", "IENUMERATION"),\
                ("CounterResetSource", "IENUMERATION"),\
                ("CounterReset", "ICOMMAND"),\
                ("CounterValue", "IINTEGER"),\
                ("CounterCurrentValue", "IINTEGER"),\
                ("Gain", "IFLOAT"),\
                ("GainAuto", "IENUMERATION"),\
                ("AutoGainLowerLimit", "IFLOAT"),\
                ("AutoGainUpperLimit", "IFLOAT"),\
                ("ADCGainEnable", "IBOOLEAN"),\
                ("DigitalShift", "IFLOAT"),\
                ("DigitalShiftEnable", "IBOOLEAN"),\
                ("Brightness", "IINTEGER"),\
                ("BlackLevel", "IINTEGER"),\
                ("BlackLevelEnable", "IBOOLEAN"),\
                ("BlackLevelAuto", "IENUMERATION"),\
                ("BalanceWhiteAuto", "IENUMERATION"),\
                ("BalanceRatioSelector", "IENUMERATION"),\
                ("BalanceRatio", "IINTEGER"),\
                ("Gamma", "IFLOAT"),\
                ("GammaSelector", "IENUMERATION"),\
                ("GammaEnable", "IBOOLEAN"),\
                ("Sharpness", "IINTEGER"),\
                ("SharpnessEnable", "IBOOLEAN"),\
                ("SharpnessAuto", "IENUMERATION"),\
                ("Hue", "IINTEGER"),\
                ("HueEnable", "IBOOLEAN"),\
                ("HueAuto", "IENUMERATION"),\
                ("Saturation", "IINTEGER"),\
                ("SaturationEnable", "IBOOLEAN"),\
                ("SaturationAuto", "IENUMERATION"),\
                ("DigitalNoiseReductionMode", "IENUMERATION"),\
                ("NoiseReduction", "IINTEGER"),\
                ("AirspaceNoiseReduction", "IINTEGER"),\
                ("TemporalNoiseReduction", "IINTEGER"),\
                ("AutoFunctionAOISelector", "IENUMERATION"),\
                ("AutoFunctionAOIWidth", "IINTEGER"),\
                ("AutoFunctionAOIHeight", "IINTEGER"),\
                ("AutoFunctionAOIOffsetX", "IINTEGER"),\
                ("AutoFunctionAOIOffsetY", "IINTEGER"),\
                ("AutoFunctionAOIUsageIntensity", "IBOOLEAN"),\
                ("AutoFunctionAOIUsageWhiteBalance", "IBOOLEAN"),\
                ("LUTSelector", "IENUMERATION"),\
                ("LUTEnable", "IBOOLEAN"),\
                ("LUTIndex", "IINTEGER"),\
                ("LUTValue", "IINTEGER"),\
                ("LUTValueAll", "REGISTER"),\
                ("EncoderSelector", "IENUMERATION"),\
                ("EncoderSourceA", "IENUMERATION"),\
                ("EncoderSourceB", "IENUMERATION"),\
                ("EncoderTriggerMode", "IENUMERATION"),\
                ("EncoderCounterMode", "IENUMERATION"),\
                ("EncoderCounter", "IINTEGER"),\
                ("EncoderCounterMax", "IINTEGER"),\
                ("EncoderCounterReset", "ICOMMAND"),\
                ("EncoderMaxReverseCounter", "IINTEGER"),\
                ("EncoderReverseCounterReset", "ICOMMAND"),\
                ("InputSource", "IENUMERATION"),\
                ("SignalAlignment", "IENUMERATION"),\
                ("PreDivider", "IINTEGER"),\
                ("Multiplier", "IINTEGER"),\
                ("PostDivider", "IINTEGER"),\
                ("ShadingSelector", "IENUMERATION"),\
                ("ActivateShading", "ICOMMAND"),\
                ("NUCEnable", "IBOOLEAN"),\
                ("FPNCEnable", "IBOOLEAN"),\
                ("PRNUCEnable", "IBOOLEAN"),\
                ("UserSetCurrent", "IINTEGER"),\
                ("UserSetSelector", "IENUMERATION"),\
                ("UserSetLoad", "ICOMMAND"),\
                ("UserSetSave", "ICOMMAND"),\
                ("UserSetDefault", "IENUMERATION"),\
                ("PayloadSize", "IINTEGER"),\
                ("GevVersionMajor", "IINTEGER"),\
                ("GevVersionMinor", "IINTEGER"),\
                ("GevDeviceModeIsBigEndian", "IBOOLEAN"),\
                ("GevDeviceModeCharacterSet", "IENUMERATION"),\
                ("GevInterfaceSelector", "IINTEGER"),\
                ("GevMACAddress", "IINTEGER"),\
                ("GevSupportedOptionSelector", "IENUMERATION"),\
                ("GevSupportedOption", "IBOOLEAN"),\
                ("GevCurrentIPConfigurationLLA", "IBOOLEAN"),\
                ("GevCurrentIPConfigurationDHCP", "IBOOLEAN"),\
                ("GevCurrentIPConfigurationPersistentIP", "IBOOLEAN"),\
                ("GevPAUSEFrameReception", "IBOOLEAN"),\
                ("GevCurrentIPAddress", "IINTEGER"),\
                ("GevCurrentSubnetMask", "IINTEGER"),\
                ("GevCurrentDefaultGateway", "IINTEGER"),\
                ("GevFirstURL", "ISTRING"),\
                ("GevSecondURL", "ISTRING"),\
                ("GevNumberOfInterfaces", "IINTEGER"),\
                ("GevPersistentIPAddress", "IINTEGER"),\
                ("GevPersistentSubnetMask", "IINTEGER"),\
                ("GevPersistentDefaultGateway", "IINTEGER"),\
                ("GevLinkSpeed", "IINTEGER"),\
                ("GevMessageChannelCount", "IINTEGER"),\
                ("GevStreamChannelCount", "IINTEGER"),\
                ("GevHeartbeatTimeout", "IINTEGER"),\
                ("GevGVCPHeartbeatDisable", "IBOOLEAN"),\
                ("GevTimestampTickFrequency", "IINTEGER"),\
                ("GevTimestampControlLatch", "ICOMMAND"),\
                ("GevTimestampControlReset", "ICOMMAND"),\
                ("GevTimestampControlLatchReset", "ICOMMAND"),\
                ("GevTimestampValue", "IINTEGER"),\
                ("GevCCP", "IENUMERATION"),\
                ("GevStreamChannelSelector", "IINTEGER"),\
                ("GevSCPInterfaceIndex", "IINTEGER"),\
                ("GevSCPHostPort", "IINTEGER"),\
                ("GevSCPDirection", "IINTEGER"),\
                ("GevSCPSFireTestPacket", "IBOOLEAN"),\
                ("GevSCPSDoNotFragment", "IBOOLEAN"),\
                ("GevSCPSBigEndian", "IBOOLEAN"),\
                ("PacketUnorderSupport", "IBOOLEAN"),\
                ("GevSCPSPacketSize", "IINTEGER"),\
                ("GevSCPD", "IINTEGER"),\
                ("GevSCDA", "IINTEGER"),\
                ("GevSCSP", "IINTEGER"),\
                ("TLParamsLocked", "IINTEGER"))