import datetime
import json
import sys
import os
import cv2
sys.path.insert(0,os.path.realpath('Views'))
sys.path.insert(0,os.path.realpath('Lib'))
sys.path.insert(0,os.path.realpath('Files'))

from multyDecode import Multy_Decode
from Client_Http import *
from parameter import *
from File import *

from PyQt5.QtWidgets import QMainWindow
class ControlerClass():
    def __init__(self):
        self.parameter=parameterClass()
        self.server_ip=self.parameter.serverIP
        self.server_port=self.parameter.server_Port
        self.fileclass=FileClass()        
        self.captureimage=False
        # pcl parameter
        self.path_to_imgage=''
        self.Z_value=0
        self.Y_value=0
        self.z_value_view=0
        self.y_value_view=0
        self.Distange_triger=0

        self.modelName=''
        self.pathtoimgage=''
        self.de=Multy_Decode() 
    def get_users(self):  
        client=Http_client_Class(self.server_ip,self.server_port)
        jtext,check=client.get('users')
        listusers=[]  
        if check:
            if jtext['Status']=='OK':
                listusers=jtext['Result']
                
        return listusers
        
    def get_models_from_server(self): # get list model from server and create file json
        listModel=[] 
        client=Http_client_Class(self.server_ip,self.server_port)
        jtext,check=client.get('models')
        if check:
            if jtext['Status']=='OK':
                listModel=jtext['Result']
            # listModel=['a','b','c']
        return listModel
    def get_code_from_server(self): # get list model from server and create file json
        code='No'
        client=Http_client_Class(self.server_ip,self.server_port)
        jtext,check=client.get('code')
        if check:
            if jtext['Status']=='OK':
                code=jtext['Result']
        return code,check
    def get_box_fail_from_server(self): # get list model from server and create file json
        code=''
        client=Http_client_Class(self.server_ip,self.server_port)
        jtext,check=client.get('boxfail')
        if check:
            if jtext['Status']=='Fail':
                code=jtext['Result']
        return code,check

# readcode
    def read_Code(self,img):
        listCode,imge,rects=self.de.simpleDecode(img)
        return listCode,imge,rects
    def read_Code_main(self,img,template,rects):
        img_aligned,image,listCode=self.de.multy_decode(img,template,rects)
        return img_aligned,image,listCode

if __name__ == "__main__":
    c=ControlerClass()
    a=c.get_code_from_server()
    print(a)
