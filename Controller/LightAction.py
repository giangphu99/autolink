import sys
import os
sys.path.append(os.path.realpath('Lib'))
from os import close
from LightCrtl import *
from parameter import parameterClass

class LightAction():
    def __init__(self,portname):
        self.__myLight=DKZ224V4ACCom(portname)
    def connect(self):
        return self.__myLight.connect()
    def open(self):
        check=self.__myLight.open()
        return check
    def close(self):
        check=self.__myLight.close()
        return check
    def Turn_On_Light12(self): 
        # self.__myLight.ActiveOne(1,1)
        # self.__myLight.ActiveOne(2,1)
        self.__myLight.Activefour(1,1,0,0)
    def Turn_Of_Light12(self):
        # self.__myLight.ActiveOne(1,0)
        # self.__myLight.ActiveOne(2,0)
        self.__myLight.Activefour(0,0,0,0)

    def setOne(self,chanel,value):
        self.__myLight.SetOne(chanel,value)
    def set_light12(self,value1,value2):
        # self.__myLight.SetOne(chanel1,value1)
        # self.__myLight.SetOne(chanel2,value2)
        self.__myLight.SetFour(value1,value2,0,0)
    def getport(self):
        listport=self.__myLight.getComPort()
        print(listport)
        return listport
# l=LightAction()
# l.getport()
# l.connect()
# a=l.open()

# print(a)
# l.set_light12(255,255)
# l.Turn_On_Light12()
 
# time.sleep(5)
# l.Turn_Of_Light12()
# l.close()
