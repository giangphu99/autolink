import cv2
import numpy as np 
from pyzbar.pyzbar import decode, bounding_box
import imutils
from PIL import Image
from datetime import datetime
class Multy_Decode():
        
    def Decode(self,img, k_blur = 1):
        # print("scan code " + str(img.shape))
        # img = cv2.cvtColor(img,cv2.COLOR_BGR)
        sn = None
        rect = (0,0,0,0)
        # img = cv2.medianBlur(img, k_blur)
        # cv2.imwrite("test1.png", img)
        try:
            rows,cols = img.shape[:2]
            result_data = []
            for n in range(1, 5 ,1):
                if(len(result_data) > 0):
                    break
                if( n > 1):
                    resized_img= cv2.resize(img,(n*rows,n*cols))
                else:
                    resized_img = img
                for al_val in range(0,25, 2):
                    alpha = 1 + al_val*0.1
                    result_img = cv2.convertScaleAbs(resized_img, alpha=alpha, beta=0)
                    result_data = decode(result_img)
                    print('quality code in image: ',len(result_data))
                    if(len(result_data) > 0):
                        for code in result_data:
                            sn = code.data.decode("utf-8")
                        break
                
            return sn
        except Exception as err:
            print(err)
            return sn, rect
    def simpleDecode(self,image,a=0.1):
        list_sn = []
        codes = decode(image)
        print('quatity code in image: ',len(codes))
        rects=[]
        for code in codes:
            sn = code.data.decode("utf-8")
            polygon=code.polygon
            # x,y,z,h=code.polygon 
            # x=[int(x[0]*a),int(x[1]*b)]
            # y=[int(y[0]*b),int(y[1]*b)]
            # z=[int(z[0]*b),int(z[1]*a)]
            # h=[int(h[0]*a),int(h[1]*a)]
            # print(x)
            # pts = np.array([x,y,z,h],np.int32)
            # pts = np.array([polygon],np.int32)
            # cv2.polylines(image,[pts] ,isClosed = True, color=(0, 255, 0), thickness=1)
            x,y,w,h=code.rect
            xmin,ymin=int(x-w*a),int(y-h*a)
            xmax,ymax=int(x+w+w*a),int(y+h+h*a)
            rects.append([(xmin,ymin),(xmax,ymax)])
            cv2.rectangle(image,(xmin,ymin),(xmax,ymax) , color=(0, 255, 0), thickness=2)
            list_sn.append(sn)
        return list_sn,image,rects
    def templateMatching(self,image,template):
        w, h = template.shape[::-1]
        res = cv2.matchTemplate(img,template,cv2.TM_SQDIFF_NORMED)
        print(res)
        # threshold = 0.8
        # loc = np.where( res >= threshold)
        # print(loc)
        # for pt in zip(*loc[::-1]):
        #     top_left = pt
        #     bottom_right = (pt[0] + w, pt[1] + h)
            # cv.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
        print(max_val)
        top_left = min_loc
        bottom_right = (top_left[0] + w, top_left[1] + h)
        mac = (top_left,bottom_right)
        print(mac)
        return mac

    def align_images(self,image, template,rects, maxFeatures=500, keepPercent=0.5,debug=False):
        aligned = np.array([])
        # img = np.array([]) 
        # cv2.imwrite("image.png", image)
        try:
            # convert both the input image and template to grayscale
            imageGray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            templateGray = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
            # cv2.imwrite("/home/pi/test.jpg",imageGray)
            # use ORB to detect keypoints and extract (binary) local
            # invariant features
            orb = cv2.ORB_create(maxFeatures)
            (kpsA, descsA) = orb.detectAndCompute(imageGray, None)
            (kpsB, descsB) = orb.detectAndCompute(templateGray, None)
            # match the features
            method = cv2.DESCRIPTOR_MATCHER_BRUTEFORCE_HAMMING
            matcher = cv2.DescriptorMatcher_create(method)
            matches = matcher.match(descsA, descsB, None)
            # sort the matches by their distance (the smaller the distance,
            # the "more similar" the features are)
            matches = sorted(matches, key=lambda x:x.distance)
            # keep only the top matches
            keep = int(len(matches) * keepPercent)
            matches = matches[:keep]
            # check to see if we should visualize the matched keypoints
            if debug:
                matchedVis = cv2.drawMatches(image, kpsA, template, kpsB,
                    matches, None)
                matchedVis = imutils.resize(matchedVis, width=1000)
                cv2.imshow("Matched Keypoints", matchedVis)
                cv2.waitKey(0)
            # allocate memory for the keypoints (x, y)-coordinates from the
            # top matches -- we'll use these coordinates to compute our
            # homography matrix
            if(len(matches)>0):
                ptsA = np.zeros((len(matches), 2), dtype="float")
                ptsB = np.zeros((len(matches), 2), dtype="float")
                # loop over the top matches
                for (i, m) in enumerate(matches):
                    # indicate that the two keypoints in the respective images
                    # map to each other
                    ptsA[i] = kpsA[m.queryIdx].pt
                    ptsB[i] = kpsB[m.trainIdx].pt
                # compute the homography matrix between the two sets of matched
                # points
                (H, mask) = cv2.findHomography(ptsA, ptsB, method=cv2.RANSAC)
                # M=cv2.getPerspectiveTransform(ptsA,ptsB)
                # print(H)

                # use the homography matrix to align the images
                (h, w) = template.shape[:2]
                aligned = cv2.warpPerspective(image,H, (w, h))
                # cv2.warpPerspective(image, H, (w, h))
                # for rect in rects:
                #     cv2.rectangle(aligned,tuple(rect[0]),tuple(rect[1]) , color=(0, 255, 0), thickness=5)
                # print(H)
                # (h, w) = aligned.shape[:2]

                # (M, mask) = cv2.findHomography(ptsB,ptsA , method=cv2.RANSAC)
                # print(M)
                # img = cv2.warpPerspective(aligned, -H, (w,h))
            # return the aligned image
            # return aligned
        except:
                # return -1
                pass
        return aligned,image
    def simpleDecode_2(self,image):
        codes = decode(image)
        sn=None
        # print('quatity code in image: ',len(codes))
        for code in codes:
            sn = code.data.decode("utf-8")
        return sn
    def multy_decode(self,image,template,rects):
        # print('quantity rectangle:',len(rects))
        listcode=[] 
        img_aligned,imge=self.align_images(image,template,rects,debug=False)
       
        if img_aligned.shape[0]==0:
                return img_aligned,imge,listcode
        else:
            for i in rects:
                roi=img_aligned[i[0][1]:i[1][1],i[0][0]:i[1][0]]
                # cv2.imwrite('/home/pi/ImageAlign/a/'+str(datetime.datetime.now())+'.jpg',roi)
                code=self.simpleDecode_2(roi)
                if code!=None:
                    listcode.append(code)
        return img_aligned,imge,listcode
if __name__ == "__main__":
    dc=Multy_Decode()
    img = cv2.imread("/home/pi/LinkCode/models/a/a.jpg")
#     # img_pil = cv2.imread('/home/pi/ImageAlign/a/Fail2021-11-29 23:38:52.650300.jpg')
    img_test = cv2.imread("/home/pi/abc.jpg")
# #     # _,img_thresh = cv2.threshold(img,127,255,cv2.THRESH_BINARY_INV)
#     # w,h = img_pil.shape[:2]
    # rects=[  [  (1451,1093), (1699,1341)], [(1385, 895),(1789,  1021) ]]
    rects=[[(1406,1107),(1651,1352) ],[(   1336, 908 ),    (1738,    1031 )]]
    img_aligned,img = dc.align_images(img_test,img,rects,debug=False)
#     img_thresh = cv2.resize(img_aligned,(720,1080
#     ))
    # sn,image,_ = dc.simpleDecode(img)
    # print(sn)
    img_aligned = cv2.resize(img_aligned,(1080,720))
    cv2.imshow('img2',img_aligned)
    cv2.waitKey(0)
    img = cv2.resize(img,(1080,720))
    cv2.imshow('img2',img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
