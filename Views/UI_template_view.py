# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'imageViews.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_template_view_class(object):
    def setupUi(self, template_view_class):
        template_view_class.setObjectName("template_view_class")
        template_view_class.resize(904, 587)
        self.gridLayout = QtWidgets.QGridLayout(template_view_class)
        self.gridLayout.setObjectName("gridLayout")
        self.vlayout = QtWidgets.QVBoxLayout()
        self.vlayout.setObjectName("vlayout")
        self.gridLayout.addLayout(self.vlayout, 0, 0, 1, 1)

        self.retranslateUi(template_view_class)
        QtCore.QMetaObject.connectSlotsByName(template_view_class)

    def retranslateUi(self, template_view_class):
        _translate = QtCore.QCoreApplication.translate
        template_view_class.setWindowTitle(_translate("template_view_class", "Dialog"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    template_view_class = QtWidgets.QDialog()
    ui = Ui_template_view_class()
    ui.setupUi(template_view_class)
    template_view_class.show()
    sys.exit(app.exec_())

