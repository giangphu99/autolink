# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'setting.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Setting_control(object):
    def setupUi(self, Setting_control):
        Setting_control.setObjectName("Setting_control")
        Setting_control.resize(637, 550)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../Icons/fii_.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Setting_control.setWindowIcon(icon)
        Setting_control.setStyleSheet("background:rgb(245, 240, 239)")
        self.groupBox = QtWidgets.QGroupBox(Setting_control)
        self.groupBox.setGeometry(QtCore.QRect(30, 130, 581, 80))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.groupBox.setFont(font)
        self.groupBox.setAutoFillBackground(False)
        self.groupBox.setStyleSheet("background:rgb(255, 255, 255)")
        self.groupBox.setObjectName("groupBox")
        self.label_6 = QtWidgets.QLabel(self.groupBox)
        self.label_6.setGeometry(QtCore.QRect(8, 35, 21, 21))
        self.label_6.setObjectName("label_6")
        self.le_plc_port = QtWidgets.QLineEdit(self.groupBox)
        self.le_plc_port.setGeometry(QtCore.QRect(290, 30, 131, 32))
        self.le_plc_port.setObjectName("le_plc_port")
        self.le_pcl_ip = QtWidgets.QLineEdit(self.groupBox)
        self.le_pcl_ip.setGeometry(QtCore.QRect(40, 30, 201, 32))
        self.le_pcl_ip.setObjectName("le_pcl_ip")
        self.label_3 = QtWidgets.QLabel(self.groupBox)
        self.label_3.setGeometry(QtCore.QRect(250, 35, 31, 22))
        self.label_3.setObjectName("label_3")
        self.btnconnectPLC = QtWidgets.QPushButton(self.groupBox)
        self.btnconnectPLC.setGeometry(QtCore.QRect(440, 30, 81, 30))
        self.btnconnectPLC.setStyleSheet("background:rgb(0, 85, 0);border-radius: 5px;\n"
"color:rgb(255, 255, 255)")
        self.btnconnectPLC.setObjectName("btnconnectPLC")
        self.lbCheckPLC = QtWidgets.QLabel(self.groupBox)
        self.lbCheckPLC.setGeometry(QtCore.QRect(540, 30, 31, 31))
        self.lbCheckPLC.setStyleSheet("background: transparent;")
        self.lbCheckPLC.setText("")
        self.lbCheckPLC.setPixmap(QtGui.QPixmap("autolink/Icons/-icon.png"))
        self.lbCheckPLC.setScaledContents(True)
        self.lbCheckPLC.setObjectName("lbCheckPLC")
        self.groupBox_2 = QtWidgets.QGroupBox(Setting_control)
        self.groupBox_2.setGeometry(QtCore.QRect(30, 230, 581, 80))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.groupBox_2.setFont(font)
        self.groupBox_2.setAutoFillBackground(False)
        self.groupBox_2.setStyleSheet("background:rgb(255, 255, 255)")
        self.groupBox_2.setObjectName("groupBox_2")
        self.label_4 = QtWidgets.QLabel(self.groupBox_2)
        self.label_4.setGeometry(QtCore.QRect(10, 32, 31, 22))
        self.label_4.setObjectName("label_4")
        self.le_server_ip = QtWidgets.QLineEdit(self.groupBox_2)
        self.le_server_ip.setGeometry(QtCore.QRect(40, 30, 201, 32))
        self.le_server_ip.setObjectName("le_server_ip")
        self.btnconnect_server = QtWidgets.QPushButton(self.groupBox_2)
        self.btnconnect_server.setGeometry(QtCore.QRect(440, 30, 81, 30))
        self.btnconnect_server.setStyleSheet("background:rgb(0, 85, 0);border-radius: 5px;\n"
"color:rgb(255, 255, 255)")
        self.btnconnect_server.setObjectName("btnconnect_server")
        self.le_server_port = QtWidgets.QLineEdit(self.groupBox_2)
        self.le_server_port.setGeometry(QtCore.QRect(290, 30, 131, 32))
        self.le_server_port.setObjectName("le_server_port")
        self.label_5 = QtWidgets.QLabel(self.groupBox_2)
        self.label_5.setGeometry(QtCore.QRect(250, 35, 31, 22))
        self.label_5.setObjectName("label_5")
        self.lbCheck_server = QtWidgets.QLabel(self.groupBox_2)
        self.lbCheck_server.setGeometry(QtCore.QRect(540, 30, 31, 31))
        self.lbCheck_server.setStyleSheet("\n"
"background: transparent;")
        self.lbCheck_server.setText("")
        self.lbCheck_server.setPixmap(QtGui.QPixmap("autolink/Icons/-icon.png"))
        self.lbCheck_server.setScaledContents(True)
        self.lbCheck_server.setObjectName("lbCheck_server")
        self.groupBox_3 = QtWidgets.QGroupBox(Setting_control)
        self.groupBox_3.setGeometry(QtCore.QRect(30, 40, 581, 80))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.groupBox_3.setFont(font)
        self.groupBox_3.setAutoFillBackground(False)
        self.groupBox_3.setStyleSheet("background:rgb(255, 255, 255)")
        self.groupBox_3.setObjectName("groupBox_3")
        self.btnconnectLight = QtWidgets.QPushButton(self.groupBox_3)
        self.btnconnectLight.setGeometry(QtCore.QRect(440, 30, 81, 30))
        self.btnconnectLight.setStyleSheet("background:rgb(0, 85, 0);border-radius: 5px;\n"
"color:rgb(255, 255, 255)")
        self.btnconnectLight.setObjectName("btnconnectLight")
        self.cblightPort = QtWidgets.QComboBox(self.groupBox_3)
        self.cblightPort.setGeometry(QtCore.QRect(50, 30, 371, 31))
        self.cblightPort.setStyleSheet("background:rgb(255, 255, 255)")
        self.cblightPort.setObjectName("cblightPort")
        self.label = QtWidgets.QLabel(self.groupBox_3)
        self.label.setGeometry(QtCore.QRect(0, 30, 41, 22))
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.lbcheckLight = QtWidgets.QLabel(self.groupBox_3)
        self.lbcheckLight.setGeometry(QtCore.QRect(540, 30, 31, 31))
        self.lbcheckLight.setStyleSheet("background: transparent;")
        self.lbcheckLight.setText("")
        self.lbcheckLight.setPixmap(QtGui.QPixmap("autolink/Icons/-icon.png"))
        self.lbcheckLight.setScaledContents(True)
        self.lbcheckLight.setObjectName("lbcheckLight")
        self.btnsave = QtWidgets.QPushButton(Setting_control)
        self.btnsave.setGeometry(QtCore.QRect(300, 460, 61, 30))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        self.btnsave.setFont(font)
        self.btnsave.setStyleSheet("background:rgb(0, 85, 0);border-radius: 5px;\n"
"color:rgb(255, 255, 255)")
        self.btnsave.setObjectName("btnsave")
        self.groupBox_4 = QtWidgets.QGroupBox(Setting_control)
        self.groupBox_4.setGeometry(QtCore.QRect(30, 330, 581, 81))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.groupBox_4.setFont(font)
        self.groupBox_4.setAutoFillBackground(False)
        self.groupBox_4.setStyleSheet("background:rgb(255, 255, 255)")
        self.groupBox_4.setObjectName("groupBox_4")
        self.label_7 = QtWidgets.QLabel(self.groupBox_4)
        self.label_7.setGeometry(QtCore.QRect(40, 40, 131, 22))
        self.label_7.setObjectName("label_7")
        self.le_working_distance = QtWidgets.QLineEdit(self.groupBox_4)
        self.le_working_distance.setGeometry(QtCore.QRect(180, 38, 251, 32))
        self.le_working_distance.setObjectName("le_working_distance")
        self.label_2 = QtWidgets.QLabel(self.groupBox_4)
        self.label_2.setGeometry(QtCore.QRect(460, 40, 31, 22))
        self.label_2.setObjectName("label_2")

        self.retranslateUi(Setting_control)
        QtCore.QMetaObject.connectSlotsByName(Setting_control)

    def retranslateUi(self, Setting_control):
        _translate = QtCore.QCoreApplication.translate
        Setting_control.setWindowTitle(_translate("Setting_control", "Setting"))
        self.groupBox.setTitle(_translate("Setting_control", "PLC:"))
        self.label_6.setText(_translate("Setting_control", "IP"))
        self.label_3.setText(_translate("Setting_control", "Port"))
        self.btnconnectPLC.setText(_translate("Setting_control", "connect"))
        self.groupBox_2.setTitle(_translate("Setting_control", "Server:"))
        self.label_4.setText(_translate("Setting_control", "IP"))
        self.btnconnect_server.setText(_translate("Setting_control", "connect"))
        self.label_5.setText(_translate("Setting_control", "Port"))
        self.groupBox_3.setTitle(_translate("Setting_control", "Light:"))
        self.btnconnectLight.setText(_translate("Setting_control", "connect"))
        self.label.setText(_translate("Setting_control", " Port"))
        self.btnsave.setText(_translate("Setting_control", "Save"))
        self.groupBox_4.setTitle(_translate("Setting_control", "Camera:"))
        self.label_7.setText(_translate("Setting_control", "Working Distance"))
        self.label_2.setText(_translate("Setting_control", "mm"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Setting_control = QtWidgets.QDialog()
    ui = Ui_Setting_control()
    ui.setupUi(Setting_control)
    Setting_control.show()
    sys.exit(app.exec_())

